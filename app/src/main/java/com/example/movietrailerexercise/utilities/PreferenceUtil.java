package com.example.movietrailerexercise.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.movietrailerexercise.R;

public class PreferenceUtil {

    public static SharedPreferences getMovieFavoritesSharedPreference(boolean isTV,
                                                                      Context context ) {
        String fileName= isTV ?
                context.getString(R.string.tv_favorites_shared_preference) :
                context.getString(R.string.movie_favorites_shared_preference);

        return context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    public static SharedPreferences getPeopleFavoritesSharedPreference(Context context) {

        String fileName = context.getString(R.string.people_favorites_shared_preference);
        return context.getSharedPreferences(fileName, Context.MODE_PRIVATE);

    }

    public static String getMovieFavoritesListKey(boolean isTVSelected, Context context )
    {
        return  isTVSelected ?
                context.getString(R.string.tv_ids_favorites_preference_key) :
                context.getString(R.string.movie_ids_favorites_preference_key);
    }

/*    public static String getCastsFavoritesListKey(boolean isCast, Context context) {
        return  isCast ?
                context.getString(R.string.cast_id_favorites_key) :
                context.getString(R.string.director_id_favorites_key);
    }*/

    public static boolean getSwitchMovietoTVFlag(Context context) {
        boolean switchToTVDefault =
                context.getResources().getBoolean(R.bool.switch_to_tv_tabs_default);
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                                                context.getString(R.string.movie_tv_preference),
                                                Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(context.getString(R.string.switch_to_tv_pref_key),
                                            switchToTVDefault);
    }

    public static void setSwitchMovieToTVFlag(Context context, boolean switchMovieToTVFlag) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.movie_tv_preference),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean( context.getString(R.string.switch_to_tv_pref_key), switchMovieToTVFlag);
        editor.commit();
    }
}
