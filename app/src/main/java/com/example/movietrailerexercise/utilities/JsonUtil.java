package com.example.movietrailerexercise.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.example.movietrailerexercise.data.Person;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.data.MovieSearchConstants;
import com.example.movietrailerexercise.data.QueryResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JsonUtil
{
    private static final String TAG = "JsonUtil";
    private static final int MAX_NUM_OF_CASTS = 18;


    public static Movie extractLatestMovieData(String urlResponse )
            throws JSONException, IOException
    {
        JSONObject data = new JSONObject( urlResponse );
        Movie latestMovie = new Movie();

        // get and set movie ID
        int movieID = data.getInt( MovieSearchConstants.JSON_MOVIE_ID_KEY );
        latestMovie.setMovieID( movieID );

        // get and set movie title
        String movieTitle;
/*        if ( isTVSelected )
        {
            movieTitle = data.getString(MovieSearchConstants.JSON_TV_TITLE_KEY);
        }
        else
        {
            movieTitle = data.getString(MovieSearchConstants.JSON_MOVIE_TITLE_KEY);
        }*/
        //TODO: Update the code to be able to support TV latest when user toggle to Switch TV
        movieTitle = data.getString(MovieSearchConstants.JSON_MOVIE_TITLE_KEY);
        latestMovie.setMovieTitle( movieTitle );

        // get and set movie poster
        // if there is no movie poster, then discard this data, and continue to next one
        String posterPath = data.getString(MovieSearchConstants.JSON_POSTER_PATH_KEY);
/*        if ( posterPath.equals("null") || posterPath.length() == 0 )
        {
            latestMovie.setMoviePoster( null );
        }
        else {
            String completePosterPath = MovieSearchConstants.MOVIE_DB_POSTER_DOMAIN + posterPath;
            latestMovie.setMoviePoster(decodeMovieImagePath(completePosterPath));
        }*/
        if ( posterPath.equals("null") || posterPath.length() == 0 )
        {
            latestMovie.setPosterPath( null );
        }
        else {
            String completePosterPath = MovieSearchConstants.MOVIE_DB_POSTER_DOMAIN + posterPath;
            latestMovie.setPosterPath(completePosterPath);
        }

        return latestMovie;
    }

    private static List<String> getMovieGenresFromJSON( JSONObject fullResults )
            throws JSONException
    {
        JSONArray genresResult =
                fullResults.getJSONArray(MovieSearchConstants.JSON_DETAIL_MOVIE_GENRES_ARRAY_KEY);
        int numOfGenres = genresResult.length();
        List<String> genresList = new ArrayList<>();
        for (int i=0; i<numOfGenres; i++)
        {
            String genre = genresResult.getJSONObject(i)
                    .getString(MovieSearchConstants.JSON_MOVIE_DETAIL_GENRE_NAME_KEY);
            genresList.add(genre);
        }

        return genresList;
    }

    /**
     * This method decodes the given image path, and return the equivalent Bitmap image
     * MUST be called in a background thread!
     *
     * @param posterPath The image (poster) path
     * @return Bitmap The image itself
     */
    public static Bitmap decodeMovieImagePath(String posterPath )
    {
        InputStream in = null;
        try {
            in = new URL(posterPath).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeStream(in);
    }

    private static List<Person> getMovieDirectors(JSONObject fullResults, boolean isTVSelected )
            throws JSONException
    {
        JSONObject creditsResult =
                fullResults.getJSONObject(MovieSearchConstants.JSON_CREDITS_RESULT_KEY);
        JSONArray crewList = creditsResult.getJSONArray(MovieSearchConstants.JSON_ARRAY_CREW_KEY);

        if ( crewList == null || crewList.length() == 0 )
        {
            return null;
        }
        List<Person> directorsList = new ArrayList<>();
        for ( int i=0; i<crewList.length(); i++ )
        {
            String crewJob =
                    crewList.getJSONObject(i).getString(MovieSearchConstants.JSON_CREW_JOB_KEY);
            if ( crewJob.equalsIgnoreCase(MovieSearchConstants.DIRECTOR_JOB)) {
                Person director = getCreditDataObject(crewList.getJSONObject(i));
                directorsList.add(director);
            }
        }

        return directorsList;
    }

    private static List<Person> getMovieCasts(JSONObject fullResults, boolean isTVSelected )
            throws Exception
    {
        JSONObject creditsResult =
                fullResults.getJSONObject(MovieSearchConstants.JSON_CREDITS_RESULT_KEY);
        JSONArray castsList = creditsResult.getJSONArray(MovieSearchConstants.JSON_ARRAY_CASTS_KEY);
        // check the number of people returned in JSON array. If the number of people is larger than
        // hardcoded max number of people (5 for now), then we just pick the top 5 people. Otherwise,
        // retrieve all people name.
        int numOfCasts = castsList.length();
        int maxNumOfCasts =  (numOfCasts>MAX_NUM_OF_CASTS) ? MAX_NUM_OF_CASTS : numOfCasts;

        List<Person> people = new ArrayList<>();

        for ( int i=0; i<maxNumOfCasts; i++ ) {
            Person actor = getCreditDataObject(castsList.getJSONObject(i));
            people.add(actor);
        }

        return people;
    }

    private static Person getCreditDataObject(JSONObject data) throws JSONException {
        Person person = new Person();

        // get credit's name
        String creditName = data.getString(MovieSearchConstants.JSON_COMMON_NAME_KEY);
        person.setName(creditName);

        // get credit's portrait path
        String creditPortrait = data.getString(MovieSearchConstants.JSON_CASTS_PORTRAIT_PATH_KEY);
        if (creditPortrait == null || creditPortrait.equals("null")) {
            person.setPortraitPath(null);
        }
        else {
            person.setPortraitPath(MovieSearchConstants.MOVIE_DB_POSTER_DOMAIN + creditPortrait);
        }

        // get credit's ID
        int creditID = data.getInt(MovieSearchConstants.JSON_COMMON_ID_KEY);
        person.setCastID(creditID);

        return person;
    }

    private static Uri getMovieTrailerPath(JSONObject fullResult) throws Exception
    {
        JSONObject videoQueryResult =
                fullResult.getJSONObject(MovieSearchConstants.JSON_VIDEOS_RESULT_KEY);
        JSONArray videoArrayResult =
                videoQueryResult.getJSONArray(MovieSearchConstants.JSON_ARRAY_RESULTS_KEY);
        if ( videoArrayResult == null || videoArrayResult.length() == 0 )
        {
            return null;
        }
        // just get the first result idx
        JSONObject videoObject = videoArrayResult.getJSONObject(0);
        String youTubeKey = videoObject.getString(MovieSearchConstants.JSON_VIDEO_YOUTUBE_KEY);
        if ( youTubeKey.equals("null") || youTubeKey.length() < 1 )
        {
            return null;
        }

        return QueryUtil.buildMovieTrailerPath(youTubeKey);
    }

    private static ArrayList<Movie> populateSimpleMovieDataList( JSONArray movieResultsArray,
                                                                 boolean isTVSelected )
            throws JSONException {

        ArrayList<Movie> moviesList = new ArrayList<>();

        // get Movie for each movie in the list
        for (int i = 0; i< movieResultsArray.length(); i++ ) {

            JSONObject movieIdx = movieResultsArray.getJSONObject(i);
            Movie movie = getMovieDataObject(movieIdx, isTVSelected);
            if (movie != null) {
                moviesList.add(movie);
            }

        }

        return moviesList;
    }

    private static Movie getMovieDataObject(JSONObject movieIdx, boolean isTVSelected)
                            throws JSONException{
        // get and set movie poster
        // if there is no movie poster, then discard this data, and continue to next one
        String posterPath = movieIdx.getString(MovieSearchConstants.JSON_POSTER_PATH_KEY);
        if (posterPath == null || posterPath.equals("null") || posterPath.length() == 0) {
            return null;
        }

        Movie movie = new Movie();
        String completePosterPath = MovieSearchConstants.MOVIE_DB_POSTER_DOMAIN +
                                    posterPath;

        //movie.setMoviePoster( decodeMovieImagePath(completePosterPath) );
        movie.setPosterPath(completePosterPath);

        // get and set movie ID
        int movieID = movieIdx.getInt(MovieSearchConstants.JSON_MOVIE_ID_KEY);
        movie.setMovieID( movieID );

        // get and set vote average
        double voteAvg = movieIdx.getDouble(MovieSearchConstants.JSON_VOTE_AVERAGE_KEY);
        movie.setMovieAverageVote( voteAvg );

        // get and set movie title
        String title;
        if (isTVSelected) {
            title = movieIdx.getString(MovieSearchConstants.JSON_TV_TITLE_KEY);
        } else {
            title = movieIdx.getString(MovieSearchConstants.JSON_MOVIE_TITLE_KEY);
        }
        movie.setMovieTitle( title );

        // get and set movie Overview
        String overview = movieIdx.getString(MovieSearchConstants.JSON_OVERVIEW_KEY);
        movie.setMovieOverview(overview);

        return movie;
    }

    public static QueryResponse extractSimpleMovieDataList(String urlResponse, boolean isTVSelected )
    {
        if ( urlResponse == null ) {
            // invalid response, just return null
            return null;
        }

        try {
            //first get the whole query results in JSONObject
            JSONObject fullResults = new JSONObject( urlResponse );

            // next, get the movie list search results in JSONArray
            JSONArray movieResultsArray =
                    fullResults.getJSONArray(MovieSearchConstants.JSON_ARRAY_RESULTS_KEY);

            QueryResponse response = new QueryResponse();
            int totalPage = fullResults.getInt(MovieSearchConstants.TOTAL_PAGE_QUERY_SEARCH);
            response.setTotalPage(totalPage);

            List<Movie> movieList = populateSimpleMovieDataList(movieResultsArray, isTVSelected);
            response.setMovieList(movieList);

            return response;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Movie extractDetailMovieDataList(String urlResponse, boolean isTVSelected )
    {
        if ( urlResponse == null )
        {
            // invalid response, just return null
            return null;
        }

        Movie movie = new Movie();
        try
        {
            //first get the whole query results in JSONObject
            JSONObject fullResults = new JSONObject( urlResponse );

            // get and set movie genres
            movie.setGenreList( getMovieGenresFromJSON(fullResults) );

            // get movie homepage
            String homepage = fullResults.getString(MovieSearchConstants.JSON_COMMON_HOMEPAGE_KEY);
            movie.setHomepage( homepage );

            // get movie ID
            int movieID = fullResults.getInt(MovieSearchConstants.JSON_MOVIE_ID_KEY);
            movie.setMovieID( movieID );

            // get movie overview
            String overview = fullResults.getString(MovieSearchConstants.JSON_OVERVIEW_KEY);
            if ( overview.length() == 0 )
            {
                overview = null;
            }
            movie.setMovieOverview(overview);

            // get movie release date
            String releaseDate;
            if ( isTVSelected )
            {
                releaseDate = fullResults.getString(MovieSearchConstants.JSON_TV_LAST_AIR_DATE_KEY);
            }
            else
            {
                releaseDate = fullResults.getString(MovieSearchConstants.JSON_RELEASE_DATE_KEY);
            }
            movie.setReleaseDate( releaseDate );

            // get movie title
            String movieTitle;
            if ( isTVSelected ) {
                movieTitle = fullResults.getString(MovieSearchConstants.JSON_TV_TITLE_KEY);
            } else {
                movieTitle = fullResults.getString(MovieSearchConstants.JSON_MOVIE_TITLE_KEY);
            }
            movie.setMovieTitle( movieTitle );

            // get vote average
            double voteAvg = fullResults.getDouble(MovieSearchConstants.JSON_VOTE_AVERAGE_KEY);
            movie.setMovieAverageVote( voteAvg );

            // get vote count
            int voteCount = fullResults.getInt(MovieSearchConstants.JSON_VOTE_COUNT_KEY);
            movie.setVoteCount( voteCount );

            // get movie casts
            movie.setActorList( getMovieCasts(fullResults, isTVSelected) );

            // get movie directors
            movie.setDirectorList( getMovieDirectors(fullResults, isTVSelected));

            // get trailer youtube path
            movie.setTrailerPath( getMovieTrailerPath(fullResults) );

            // get movie poster path
            String posterPath = fullResults.getString(MovieSearchConstants.JSON_POSTER_PATH_KEY);
            String completePosterPath;
            if ( posterPath.length() > 0 ) {
                completePosterPath =
                        MovieSearchConstants.MOVIE_DB_HIGH_QUALITY_POSTER_DOMAIN + posterPath;
            }
            else
            {
                completePosterPath = null;
            }
            movie.setPosterPath(completePosterPath);

            // get similar movies
            JSONObject similarMoviesResult =
                    fullResults.getJSONObject(MovieSearchConstants.JSON_MOVIES_SIMILAR_ID_KEY);
            // next, get the movie list search results in JSONArray
            JSONArray movieResultsArray =
                    similarMoviesResult.getJSONArray(MovieSearchConstants.JSON_ARRAY_RESULTS_KEY);
            List<Movie> similarMovies = populateSimpleMovieDataList(movieResultsArray, isTVSelected);

            movie.setSimilarMovies(similarMovies);

            String status = fullResults.getString(MovieSearchConstants.JSON_STATUS_RESULT_KEY);
            movie.setStatus(status);

            // specific for tvs
            if (isTVSelected) {
                String firstAirDate = fullResults.getString(MovieSearchConstants.JSON_FIRST_AIR_DATE_KEY);
                movie.setFirstAirDate(firstAirDate);

                int numOfEpisodes = fullResults.getInt(MovieSearchConstants.JSON_NUMBER_OF_EPISODES_KEY);
                movie.setNumberOfEpisodes(numOfEpisodes);

                int numOfSeasons = fullResults.getInt(MovieSearchConstants.JSON_NUMBER_OF_SEASONS_KEY);
                movie.setNumberOfSeasons(numOfSeasons);

                int episodeRuntime =
                        fullResults.getJSONArray(MovieSearchConstants.JSON_TV_RUNTIME_RESULT_KEY)
                                    .getInt(0);
                movie.setRuntime(episodeRuntime);
            } else {
                // specific for movies
                movie.setSpokenLanguage(extractSpokenLanguages(fullResults));

                int revenue = fullResults.getInt(MovieSearchConstants.JSON_REVENUE_RESULT_KEY);
                movie.setRevenue(revenue);

                int budget = fullResults.getInt(MovieSearchConstants.JSON_BUDGET_RESULT_KEY);
                movie.setBudget(budget);

                int runtime = fullResults.getInt(MovieSearchConstants.JSON_RUNTIME_RESULT_KEY);
                movie.setRuntime(runtime);

            }

        } catch (Exception e) {
            e.printStackTrace();
            //return null;
            return movie;
        }

        return movie;
    }

    private static List<String> extractSpokenLanguages(JSONObject fullResults)
            throws JSONException {

        List<String> spokenLanguages = new ArrayList<>();
        JSONArray resultsArray =
                fullResults.getJSONArray(MovieSearchConstants.JSON_SPOKEN_LANGUAGE_RESULT_KEY);
        for (int i=0; i<resultsArray.length(); i++) {
            String language =
                    resultsArray.getJSONObject(i).getString(MovieSearchConstants.JSON_COMMON_NAME_KEY);
            spokenLanguages.add(language);
        }
        return spokenLanguages;
    }

    public static Person extractCastDetailData(String urlResponse ) {
        try{
            if (urlResponse == null) {
                return null;
            }

            JSONObject fullResults = new JSONObject(urlResponse);
            Person person = new Person();

            // get name
            String name = fullResults.getString(MovieSearchConstants.JSON_COMMON_NAME_KEY);
            person.setName(name);

            // get birthday
            String birthday = fullResults.getString(MovieSearchConstants.JSON_CASTS_BIRTHDAY_KEY);
            person.setBirthday(birthday);

            // get profession
            String profession = fullResults.getString(MovieSearchConstants.JSON_CASTS_JOB_PROFESSION_KEY);
            person.setProfession(profession);

            // get ID
            int id = fullResults.getInt(MovieSearchConstants.JSON_COMMON_ID_KEY);
            person.setCastID(id);

            // get biography
            String biography = fullResults.getString(MovieSearchConstants.JSON_CASTS_BIOGRAPHY_KEY);
            person.setBiography(biography);

            // get place of birth
            String placeOfBirth = fullResults.getString(MovieSearchConstants.JSON_CASTS_PLACE_OF_BIRTH_KEY);
            person.setBirthPlace(placeOfBirth);

            // get profile picture
            String profilePicturePath = fullResults.getString(MovieSearchConstants.JSON_CASTS_PORTRAIT_PATH_KEY);
            person.setPortraitPath(MovieSearchConstants.MOVIE_DB_POSTER_DOMAIN + profilePicturePath);

            // get homepage
            String homepage = fullResults.getString(MovieSearchConstants.JSON_COMMON_HOMEPAGE_KEY);
            person.setHomepage(homepage);

            // get Movies list known for this person/director
            JSONObject movieCreditsResults =
                    fullResults.getJSONObject(MovieSearchConstants.JSON_CASTS_MOVIE_CREDITS_KEY);
            JSONArray movieCreditsArray =
                    movieCreditsResults.getJSONArray(MovieSearchConstants.JSON_ARRAY_CASTS_KEY);
            List<Movie> movieList = populateSimpleMovieDataList(movieCreditsArray, false);
            person.setMoviesList(movieList);

            // get TV list known for this person/director
            JSONObject tvCreditsResults =
                    fullResults.getJSONObject(MovieSearchConstants.JSON_CASTS_TV_CREDITS_KEY);
            JSONArray tvCreditsArray =
                    tvCreditsResults.getJSONArray(MovieSearchConstants.JSON_ARRAY_CASTS_KEY);
            List<Movie> tvList = populateSimpleMovieDataList(tvCreditsArray, true);
            person.setTVsList(tvList);

            return person;

        }catch (JSONException e )
        {
            return null;
        }
    }

    public static QueryResponse extractUserSearchResults(String searchResponse) {
        if (searchResponse == null) {
            return null;
        }

        QueryResponse queryResponse = new QueryResponse();
        List<Movie> movieList = new ArrayList<>();
        List<Movie> tvList = new ArrayList<>();
        List<Person> personList = new ArrayList<>();

        try {

            JSONObject fullResults = new JSONObject(searchResponse);
            int totalPage = fullResults.getInt(MovieSearchConstants.TOTAL_PAGE_QUERY_SEARCH);
            queryResponse.setTotalPage(totalPage);

            JSONArray resultsArray =
                    fullResults.getJSONArray(MovieSearchConstants.JSON_ARRAY_RESULTS_KEY);

            for (int i = 0; i<resultsArray.length(); i++) {
                JSONObject data = resultsArray.getJSONObject(i);
                String mediaType = data.getString(MovieSearchConstants.JSON_SEARCH_MEDIA_TYPE_KEY);
                switch (mediaType) {
                    case MovieSearchConstants.MEDIA_TYPE_PERSON:
                        Person credit = getCreditDataObject(data);
                        if (credit != null) {
                            personList.add(credit);
                        }
                        break;

                    case MovieSearchConstants.MEDIA_TYPE_MOVIE:
                        Movie movie = getMovieDataObject(data, false);
                        if (movie != null) {
                            movieList.add(movie);
                        }
                        break;

                    case MovieSearchConstants.MEDIA_TYPE_TV:
                        Movie tv = getMovieDataObject(data, true);
                        if (tv != null) {
                            tvList.add(tv);
                        }
                        break;

                    default:
                        break;
                }
            }

            queryResponse.setMovieList(movieList);
            queryResponse.setTVList(tvList);
            queryResponse.setCastList(personList);

        } catch (JSONException e) {
            e.printStackTrace();
            return queryResponse;
        }

        return queryResponse;
    }
}
