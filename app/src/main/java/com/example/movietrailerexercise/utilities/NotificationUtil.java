package com.example.movietrailerexercise.utilities;



import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;

public class NotificationUtil
{
    private static final int LATEST_MOVIE_NOTIFICATION_ID = 3004;

    private static final String CHANNEL_ID = "latest_movie_notification";

    private static void createNotificationChannel( Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager =
                    context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static void notifyUserOfLatestMovie(Context context, Movie movie)
    {
        createNotificationChannel( context );

        Bitmap largeIcon = BitmapFactory.decodeResource(
                context.getResources(),
                R.mipmap.ic_launcher);

        String notificationTitle = movie.getMovieTitle();

        String notificationText = movie.getMovieOverview();

        android.support.v4.app.NotificationCompat.Builder notificationBuilder =
                new android.support.v4.app.NotificationCompat.Builder( context, CHANNEL_ID)
                        .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationText)
                        .setPriority(android.support.v4.app.NotificationCompat.PRIORITY_DEFAULT)
                        .setAutoCancel(true);

/*        if ( movie.getPosterPath() != null )
        {
            //Bitmap moviePoster = JsonUtil.decodeMovieImagePath( movie.getPosterPath() );
            notificationBuilder.setStyle(
                    new android.support.v4.app.NotificationCompat.BigPictureStyle()
                    .bigPicture(movie.getPosterPath()));
        }*/

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        /* LATEST_MOVIE_NOTIFICATION_ID allows you to update or cancel the notification later on */
        notificationManager.notify(LATEST_MOVIE_NOTIFICATION_ID, notificationBuilder.build());
    }
}
