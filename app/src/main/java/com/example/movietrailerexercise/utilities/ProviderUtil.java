package com.example.movietrailerexercise.utilities;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Person;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.data.MovieContract;

import java.util.HashSet;
import java.util.Set;

public class ProviderUtil
{
    public static int deleteMovie(Context context, int movieID, boolean isTVSelected )
    {
        Uri contentUri = isTVSelected? MovieContract.MovieEntry.TV_CONTENT_URI:
                                       MovieContract.MovieEntry.MOVIE_CONTENT_URI;
        Uri movieUri = ContentUris.withAppendedId(contentUri, movieID);
        int rowsDeleted =
                context.getContentResolver().delete(movieUri, null, null);
        if ( rowsDeleted > 0 ) {

            String favoritesListKey = PreferenceUtil.getMovieFavoritesListKey( isTVSelected, context );

            SharedPreferences sharePref = PreferenceUtil.getMovieFavoritesSharedPreference( isTVSelected, context );
            SharedPreferences.Editor editor = sharePref.edit();
            Set<String> currentFavoriteList = sharePref.getStringSet(favoritesListKey, null);
            if (currentFavoriteList != null) {
                Set<String> updatedFavoriteList = new HashSet<>();
                updatedFavoriteList.addAll(currentFavoriteList);
                updatedFavoriteList.remove(String.valueOf(movieID));
                editor.clear();
                editor.putStringSet(favoritesListKey, updatedFavoriteList);
                editor.commit();
            }
        }

        return rowsDeleted;
    }

    public static int deleteAllFavorites(Context context )
    {
        int totalRowsDeleted = 0;
        for (int i = 0; i < 3; i++)
        {
            Uri contentUri = null;
            boolean isTV = false;
            switch (i) {
                case 0:
                    contentUri = MovieContract.MovieEntry.MOVIE_CONTENT_URI;
                    break;
                case 1:
                    contentUri = MovieContract.MovieEntry.TV_CONTENT_URI;
                    isTV = true;
                    break;
                case 2:
                    contentUri = MovieContract.MovieEntry.PEOPLE_CONTENT_URI;
                    break;
/*                case 3:
                    contentUri = MovieContract.MovieEntry.DIRECTOR_CONTENT_URI;
                    break;*/
                default:
                    break; //should never happen
            }

            if (contentUri == null) {
                continue;
            }

            int rowsDeleted =
                    context.getContentResolver().delete(contentUri, null, null);
            if (rowsDeleted > 0) {

                SharedPreferences sharePref;
                if (i <= 1) {
                    sharePref = PreferenceUtil.getMovieFavoritesSharedPreference(isTV, context);
                }
                else {
                    sharePref = PreferenceUtil.getPeopleFavoritesSharedPreference(context);
                }

                SharedPreferences.Editor editor = sharePref.edit();
                editor.clear();
                editor.commit();
            }

            totalRowsDeleted += rowsDeleted;
        }

        return totalRowsDeleted;
    }

    public static Uri saveMovieToDatabase(Context context, Movie movie, boolean isTVSelected )
    {
        ContentValues cv = new ContentValues();
        cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_AVERAGE_VOTE, movie.getMovieAverageVote());
        cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_ID, movie.getMovieID());
        cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_TITLE, movie.getMovieTitle());

        String posterPath = movie.getPosterPath();

        cv.put(MovieContract.MovieEntry.COLUMN_MOVIE_POSTER, posterPath);

        Uri contentUri = isTVSelected? MovieContract.MovieEntry.TV_CONTENT_URI:
                MovieContract.MovieEntry.MOVIE_CONTENT_URI;

        Uri newUri = context.getContentResolver().insert(contentUri, cv );
        if ( newUri != null )
        {
            SharedPreferences sharePref = PreferenceUtil.getMovieFavoritesSharedPreference( isTVSelected, context );

            String favoritesListKey = PreferenceUtil.getMovieFavoritesListKey( isTVSelected, context );

            SharedPreferences.Editor editor = sharePref.edit();
            Set<String> newStrSet = new HashSet<String>();
            Set<String> movieIDsFavoriteList = sharePref.getStringSet(favoritesListKey,
                    new HashSet<String>() );
            newStrSet.addAll(movieIDsFavoriteList);
            newStrSet.add( String.valueOf(movie.getMovieID()));
            editor.putStringSet( favoritesListKey, newStrSet);
            editor.commit();
        }

        return newUri;
    }

    public static Uri savePersonIntoDatabase(Context context, Person person) {
        ContentValues cv = new ContentValues();
        cv.put(MovieContract.MovieEntry.COLUMN_PERSON_ID, person.getID());
        cv.put(MovieContract.MovieEntry.COLUMN_PERSON_NAME, person.getName());
        cv.put(MovieContract.MovieEntry.COLUMN_PERSON_POSTER_PATH, person.getPortraitPath());

        Uri newUri =
                context.getContentResolver().insert(MovieContract.MovieEntry.PEOPLE_CONTENT_URI, cv);

        if (newUri != null) {
            SharedPreferences sharePref = PreferenceUtil.getPeopleFavoritesSharedPreference(context);

            //String favoritesListKey = PreferenceUtil.getCastsFavoritesListKey(isCast, context);
            String favoritesListKey = context.getString(R.string.people_favorites_preference_key);

            SharedPreferences.Editor editor = sharePref.edit();
            Set<String> newStrSet = new HashSet<String>();
            Set<String> movieIDsFavoriteList = sharePref.getStringSet(favoritesListKey,
                    new HashSet<String>() );
            newStrSet.addAll(movieIDsFavoriteList);
            newStrSet.add(String.valueOf(person.getID()));
            editor.putStringSet(favoritesListKey, newStrSet);
            editor.commit();
        }

        return newUri;
    }

    public static int deletePerson(Context context, int personID)
    {
        //int castID = person.getID();
        Uri contentUri = MovieContract.MovieEntry.PEOPLE_CONTENT_URI;
        Uri movieUri = ContentUris.withAppendedId(contentUri, personID);
        int rowsDeleted =
                context.getContentResolver().delete(movieUri, null, null);
        if (rowsDeleted > 0) {

            String favoritesListKey = context.getString(R.string.people_favorites_preference_key);

            SharedPreferences sharePref = PreferenceUtil.getPeopleFavoritesSharedPreference(context);
            SharedPreferences.Editor editor = sharePref.edit();
            Set<String> currentFavoriteList = sharePref.getStringSet(favoritesListKey, null);
            if (currentFavoriteList != null) {
                Set<String> updatedFavoriteList = new HashSet<>();
                updatedFavoriteList.addAll(currentFavoriteList);
                updatedFavoriteList.remove(String.valueOf(personID));
                editor.clear();
                editor.putStringSet(favoritesListKey, updatedFavoriteList);
                editor.commit();
            }
        }

        return rowsDeleted;
    }

}
