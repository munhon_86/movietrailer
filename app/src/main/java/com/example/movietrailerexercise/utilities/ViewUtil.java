package com.example.movietrailerexercise.utilities;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.example.movietrailerexercise.R;

import java.text.DecimalFormat;

import static android.support.v4.content.ContextCompat.getColor;

public class ViewUtil
{
    private static final int NOT_AVAILABLE_RATING_SCORE = 0;
    private static final int VERY_BAD_RATING_MAX_SCORE = 40;
    private static final int BAD_RATING_MAX_SCORE = 50;
    private static final int NEUTRAL_RATING_MAX_SCORE = 60;
    private static final int DECENT_RATING_MAX_SCORE = 70;
    private static final int GOOD_RATING_MAX_SCORE = 80;

    public static void updateTextView( TextView voteAvgTextView,
                                       double voteAvgDouble,
                                       Context context )
    {
        // convert to percentage factor by multiplying by 10 since the retrieved vote average
        // from JSON has one floating point. for example 3.4, 5.6 etc etc
        int voteAvgInt = (int) ( voteAvgDouble * 10 );
        String averageVoteString;
        if ( voteAvgInt == NOT_AVAILABLE_RATING_SCORE ) {
            averageVoteString = context.getString(R.string.average_vote_not_available);
        } else {
            averageVoteString = context.getString(R.string.average_votes_format_string, voteAvgInt);
        }

        voteAvgTextView.setText( averageVoteString );

        // Get the appropriate background color based on the current earthquake magnitude
        int backgroundColor = getBackgroundColor( context, voteAvgInt );

        updateBackgroundColor( voteAvgTextView, backgroundColor );

    }

    private static void updateBackgroundColor( TextView voteAvgTextView, int color )
    {
        GradientDrawable magnitudeCircle = (GradientDrawable) voteAvgTextView.getBackground();

        // Set the color on the magnitude circle
        magnitudeCircle.setColor( color );
    }

    private static int getBackgroundColor( Context context, int voteAvgInt )
    {
        int colorResID;
        if (voteAvgInt == NOT_AVAILABLE_RATING_SCORE )
        {
            colorResID = R.color.notAvailableRating;
        }
        else if ( voteAvgInt < VERY_BAD_RATING_MAX_SCORE )
        {
            colorResID = R.color.veryBadRatingColor;
        }
        else if ( voteAvgInt < BAD_RATING_MAX_SCORE )
        {
            colorResID = R.color.badRatingColor;
        }
        else if ( voteAvgInt < NEUTRAL_RATING_MAX_SCORE )
        {
            colorResID = R.color.neutralRatingColor;
        }
        else if ( voteAvgInt < DECENT_RATING_MAX_SCORE )
        {
            colorResID = R.color.decentRatingColor;
        }
        else if ( voteAvgInt < GOOD_RATING_MAX_SCORE )
        {
            colorResID = R.color.goodRatingColor;
        }
        else
        {
            colorResID = R.color.veryGoodRatingColor;
        }

        return ContextCompat.getColor( context, colorResID );
    }

}
