package com.example.movietrailerexercise.utilities;

import android.net.Uri;
import android.os.CountDownTimer;

import com.example.movietrailerexercise.ui.MoviePagerAdaptor;
import com.example.movietrailerexercise.data.MovieSearchConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.Scanner;

public final class QueryUtil {
    private static final String TAG = "QueryUtil";
    private static final int MAX_CONNECTION_RETRY = 9;
    private static final int RATE_LIMIT_EXCEED_STATUS = 429;

    private static int sStatus;

    /**
     * This method returns the entire result from the HTTP response.
     *
     * @param url The URL to fetch the HTTP response from.
     * @return The contents of the HTTP response, null if no response
     * @throws IOException Related to network and stream reading
     */
    public static String getResponseFromHttpUrl(final URL url) throws IOException {

        final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream in;
        String response = null;
        //int retry = 0;
        int status;
        do {
            try {
                in = urlConnection.getInputStream();

                status = urlConnection.getResponseCode();

                Scanner scanner = new Scanner(in);
                scanner.useDelimiter("\\A");

                boolean hasInput = scanner.hasNext();

                if (hasInput) {
                    response = scanner.next();
                }

                scanner.close();

            } catch (IOException e) {
                status = urlConnection.getResponseCode();
            }
            finally {
                urlConnection.disconnect();
            }

        } while (status == RATE_LIMIT_EXCEED_STATUS);

        return response;
    }

    private static String readFromStream( InputStream inputStream ) throws IOException
        {
            StringBuilder output = new StringBuilder();
            if (inputStream != null) {
                InputStreamReader inputStreamReader =
                        new InputStreamReader(inputStream, Charset.forName("UTF-8"));
                BufferedReader reader = new BufferedReader(inputStreamReader);
                String line = reader.readLine();
                while (line != null) {
                    output.append(line);
                    line = reader.readLine();
                }
            }
            return output.toString();
        }

    public static URL buildSearchQuery(int movieListID,
                                       String region,
                                       String language,
                                       boolean isTVSelected,
                                       int page)
            throws MalformedURLException
    {
        if (isTVSelected) {
            return buildTVSearchQuery(movieListID, language, page);
        }
        else {
            return buildMovieSearchQuery(movieListID, language, region, page);
        }

    }

    private static URL buildMovieSearchQuery(int movieListID,
                                             String language,
                                             String region,
                                             int page)
            throws MalformedURLException
    {
        String pathToQuery = null;
        switch ( movieListID )
        {
            case MoviePagerAdaptor.NOW_PLAYING_TAB:
                pathToQuery = MovieSearchConstants.NOW_PLAYING_SEARCH;
                break;
            case MoviePagerAdaptor.UPCOMING_TAB:
                pathToQuery = MovieSearchConstants.UPCOMING_SEARCH;
                break;
            case MoviePagerAdaptor.POPULAR_TAB:
                pathToQuery = MovieSearchConstants.POPULAR_SEARCH;
                break;
            case MoviePagerAdaptor.TOP_RATED_TAB:
                pathToQuery = MovieSearchConstants.TOP_RATED_SEARCH;
                break;
            default:
                new IllegalArgumentException("Invalid pager ID exception");
        }

        Uri uri =  MovieSearchConstants.MOVIE_SEARCH_BASE_URI.buildUpon()
                .appendPath(pathToQuery)
                .appendQueryParameter(
                        MovieSearchConstants.API_KEY,
                        MovieSearchConstants.API_KEY_VALUE)
                .appendQueryParameter(MovieSearchConstants.LANGUAGE_QUERY_SEARCH, language)
                .appendQueryParameter(MovieSearchConstants.REGION_QUERY_SEARCH, region)
                .appendQueryParameter(MovieSearchConstants.PAGE_QUERY_SEARCH, String.valueOf(page))
                .build();
        return new URL( uri.toString());
    }

    private static URL buildTVSearchQuery(int movieListID, String language, int page)
            throws MalformedURLException
    {
        String pathToQuery = null;
        switch ( movieListID )
        {
            case MoviePagerAdaptor.NOW_PLAYING_TAB:
                pathToQuery = MovieSearchConstants.TV_NOW_PLAYING_SEARCH;
                break;
            case MoviePagerAdaptor.UPCOMING_TAB:
                pathToQuery = MovieSearchConstants.TV_UPCOMING_SEARCH;
                break;
            case MoviePagerAdaptor.POPULAR_TAB:
                pathToQuery = MovieSearchConstants.TV_POPULAR_SEARCH;
                break;
            case MoviePagerAdaptor.TOP_RATED_TAB:
                pathToQuery = MovieSearchConstants.TV_TOP_RATED_SEARCH;
                break;
            default:
                new IllegalArgumentException("Invalid pager ID exception");
        }

        Uri uri =  MovieSearchConstants.TV_SEARCH_BASE_URI.buildUpon()
                .appendPath(pathToQuery)
                .appendQueryParameter(
                        MovieSearchConstants.API_KEY,
                        MovieSearchConstants.API_KEY_VALUE)
                .appendQueryParameter(MovieSearchConstants.LANGUAGE_QUERY_SEARCH, language)
                .appendQueryParameter(MovieSearchConstants.PAGE_QUERY_SEARCH, String.valueOf(page))
                .build();

        return new URL( uri.toString());
    }

    public static URL buildLatestMovieSearchQuery() throws MalformedURLException
    {
        Uri searchUri = MovieSearchConstants.MOVIE_SEARCH_BASE_URI.buildUpon()
                            .appendPath(MovieSearchConstants.LATEST_QUERY_SEARCH)
                            .appendQueryParameter(
                                    MovieSearchConstants.API_KEY,
                                    MovieSearchConstants.API_KEY_VALUE)
                            .appendQueryParameter(
                                    MovieSearchConstants.APPEND_TO_RESPONSE,
                                    MovieSearchConstants.CREDITS_QUERY_SEARCH)
                            .build();

        return new URL(searchUri.toString());
    }

    public static Uri buildMovieTrailerPath( String youtubeKey )
            throws MalformedURLException
    {
        Uri trailerPath = Uri.parse(MovieSearchConstants.YOUTUBE_MOVIE_TRAILER_WEBSITE).buildUpon()
                .appendQueryParameter("v",youtubeKey)
                .build();
        return trailerPath;
    }

    public static URL buildMovieDetailQuery( int movieID, String language, boolean isTVSelected )
            throws Exception
    {
        Uri baseSearchQuery = isTVSelected? MovieSearchConstants.TV_SEARCH_BASE_URI :
                                            MovieSearchConstants.MOVIE_SEARCH_BASE_URI;
        Uri searchUri =  baseSearchQuery.buildUpon()
                .appendPath(Integer.toString(movieID))
                .appendQueryParameter(
                        MovieSearchConstants.API_KEY,
                        MovieSearchConstants.API_KEY_VALUE)
                .appendQueryParameter(MovieSearchConstants.LANGUAGE_QUERY_SEARCH, language)
                .appendQueryParameter(MovieSearchConstants.APPEND_TO_RESPONSE,
                                      MovieSearchConstants.MOVIE_DETAILS_APPENDED_QUERY_SEARCH)
                .build();

        //return new URL(searchUri.toString());
        return new URL(URLDecoder.decode(searchUri.toString(), "UTF-8"));
    }

    public static URL buildCastDetailQuery( int castID )
            throws Exception
    {
        Uri searchUri = Uri.parse(MovieSearchConstants.MOVIE_DB_PERSON_DETAIL_QUERY_BASE)
                .buildUpon()
                .appendPath(Integer.toString(castID))
                .appendQueryParameter(
                        MovieSearchConstants.API_KEY,
                        MovieSearchConstants.API_KEY_VALUE)
                .appendQueryParameter(MovieSearchConstants.APPEND_TO_RESPONSE,
                        MovieSearchConstants.CAST_DETAIlS_APPENDED_QUERY_SEARCH)
                .build();

        //return new URL(searchUri.toString());
        return new URL(URLDecoder.decode(searchUri.toString(), "UTF-8"));
    }

    public static URL buildUserSearchQuery(String query, int page)
            throws Exception {
        Uri searchUri = MovieSearchConstants.USER_SEARCH_BASE_URI
                            .buildUpon()
                            .appendQueryParameter(
                                    MovieSearchConstants.API_KEY,
                                    MovieSearchConstants.API_KEY_VALUE)
                            .appendQueryParameter(
                                    MovieSearchConstants.PAGE_QUERY_SEARCH,
                                    String.valueOf(page))
                            .appendQueryParameter(MovieSearchConstants.USER_QUERY_KEY,
                                    query)
                            .build();

        return new URL(URLDecoder.decode(searchUri.toString(), "UTF-8"));
    }

}
