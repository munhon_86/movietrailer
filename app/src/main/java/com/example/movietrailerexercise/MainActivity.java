package com.example.movietrailerexercise;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.SearchView;

import com.example.movietrailerexercise.sync.MovieLatestSyncUtil;
import com.example.movietrailerexercise.ui.FavoritesActivity;
import com.example.movietrailerexercise.ui.MoviePagerAdaptor;
import com.example.movietrailerexercise.ui.SettingsActivity;
import com.example.movietrailerexercise.utilities.PreferenceUtil;
/*import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;*/

public class MainActivity extends AppCompatActivity{
    private AppBarLayout mAppBarLayout;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private android.support.v7.widget.Toolbar mToolbar;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private ActionBar mActionBar;
    private PagerAdapter mPagerAdapter;
    private SwitchCompat mTVToggleSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = findViewById(R.id.main_activity_viewpager);
        mTabLayout = findViewById(R.id.main_activity_tablayout);
        mToolbar = findViewById(R.id.main_acitivity_toolbar);
        mAppBarLayout = findViewById(R.id.main_activity_appbar);
        mNavigationView = findViewById(R.id.main_acitivty_navigation_view);
        mDrawerLayout = findViewById(R.id.main_activity_drawer_layout);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setTitle( getString(R.string.app_name));
        setSupportActionBar( mToolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // set item as selected to persist highlight
                item.setChecked(true);
                // close drawer when item is tapped
                mDrawerLayout.closeDrawers();

                if (item.getItemId() == R.id.nav_favorites)
                {
                    Intent startFavoritesActivity =
                            new Intent( MainActivity.this, FavoritesActivity.class);
                    startActivity( startFavoritesActivity );
                }

                return true;
            }
        });

        //get the menu from the navigation view
        Menu navigationViewMenu = mNavigationView.getMenu();
        MenuItem nav_switch_to_tv = navigationViewMenu.findItem(R.id.nav_switch_to_tv);

        // get the switch view
        mTVToggleSwitch = nav_switch_to_tv.getActionView().findViewById(R.id.drawer_switch_to_tv);
        mTVToggleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                PreferenceUtil.setSwitchMovieToTVFlag(MainActivity.this, isChecked);

                mPagerAdapter.notifyDataSetChanged();

                // reload the page titles by calling setupWithViewPager
                mTabLayout.setupWithViewPager(mViewPager);

                // close drawers after done with all the steps
                mDrawerLayout.closeDrawers();
            }
        });

/*        DisplayImageOptions displayImageOptions = new DisplayImageOptions
                                                        .Builder()
                                                        //.cacheInMemory(true)
                                                        .cacheOnDisk(true)
                                                        .build();

        // Create global configuration and initialize ImageLoader with this config
        ImageLoaderConfiguration config =
                new ImageLoaderConfiguration
                        .Builder(this)
                        .defaultDisplayImageOptions(displayImageOptions)
                        .build();
        ImageLoader.getInstance().init(config);*/

        mPagerAdapter = new MoviePagerAdaptor( getSupportFragmentManager(),this );
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(MoviePagerAdaptor.MAXIMUM_NO_OF_PAGES);
        mTabLayout.setupWithViewPager( mViewPager );

        boolean switchToTVDefault = PreferenceUtil.getSwitchMovietoTVFlag(this);

        mTVToggleSwitch.setChecked(switchToTVDefault);

        MovieLatestSyncUtil.initialize(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu );

        //Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        android.support.v7.widget.SearchView searchView =
                (android.support.v7.widget.SearchView) menu.findItem(R.id.menu_search).getActionView();
        ComponentName componentName = getComponentName();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(componentName);
        searchView.setSearchableInfo(searchableInfo);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch( id )
        {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;

            case R.id.refresh_all:
                mViewPager.setAdapter(mPagerAdapter);
                //mPagerAdapter.notifyDataSetChanged();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
