package com.example.movietrailerexercise.sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.preference.PreferenceManager;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.utilities.NotificationUtil;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

public class MovieTrailerFirebaseJobService extends JobService
{
    private AsyncTask<Void,Void,Movie> mFetchLatestMovie;
    @Override
    public boolean onStartJob(JobParameters job) {
        final Context context = getApplicationContext();
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences( context );
        boolean notificationEnabled =
                sharedPreferences.getBoolean( getString(R.string.pref_enable_notifications_key),
                        getResources().getBoolean(R.bool.show_notifications_by_default));
        if ( !notificationEnabled )
        {
            return false;
        }

        mFetchLatestMovie = new AsyncTask<Void, Void, Movie>() {
            @Override
            protected Movie doInBackground(Void... voids) {
                return MovieLatestSyncTask.getLatestMovie();
            }

            @Override
            protected void onPostExecute(Movie movie) {
                if ( movie != null )
                {
                    NotificationUtil.notifyUserOfLatestMovie(context, movie);
                }
            }
        };

        mFetchLatestMovie.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        if (mFetchLatestMovie != null) {
            mFetchLatestMovie.cancel(true);
        }
        return true;
    }
}
