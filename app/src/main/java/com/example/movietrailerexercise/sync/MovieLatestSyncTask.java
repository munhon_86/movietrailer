package com.example.movietrailerexercise.sync;

import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.utilities.JsonUtil;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.net.URL;

public class MovieLatestSyncTask
{
    public static synchronized Movie getLatestMovie()
    {
        Movie latestMovie = null;
        try {
            URL queryURL = QueryUtil.buildLatestMovieSearchQuery();
            String urlResponse = QueryUtil.getResponseFromHttpUrl( queryURL );
            if ( urlResponse != null )
            {
                latestMovie = JsonUtil.extractLatestMovieData( urlResponse );
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return latestMovie;
    }
}
