package com.example.movietrailerexercise.sync;

import android.content.Context;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;

import java.util.concurrent.TimeUnit;

public class MovieLatestSyncUtil
{
    /*
     * Interval at which to sync with the weather. Use TimeUnit for convenience, rather than
     * writing out a bunch of multiplication ourselves and risk making a silly mistake.
     */
    private static final int SYNC_INTERVAL_HOURS = 3;
    private static final int SYNC_INTERVAL_DAYS = 5;
    private static final int SYNC_INTERVAL_DAYS_MARGIN = 2;
    private static final int SYNC_INTERVAL_SECONDS = (int) TimeUnit.DAYS.toSeconds(SYNC_INTERVAL_DAYS);
    private static final int SYNC_INTERVAL_SECONDS_MARGIN = (int) TimeUnit.DAYS.toSeconds(SYNC_INTERVAL_DAYS_MARGIN);
    private static final int SYNC_MAX_INTERVAL_SECONDS_WINDOW = SYNC_INTERVAL_SECONDS + SYNC_INTERVAL_SECONDS_MARGIN;
    private static final int SYNC_FLEXTIME_SECONDS = SYNC_INTERVAL_SECONDS / 3;

    private static boolean sInitialized;

    private static final String MOVIE_SYNC_TAG = "movie-sync";

    public static void initialize(Context context)
    {
        if ( sInitialized )
        {
            return;
        }

        sInitialized = true;

        scheduleFirebaseJobDispatcherSync(context);
    }

    private static void scheduleFirebaseJobDispatcherSync(Context context)
    {
        Driver googleDriver = new GooglePlayDriver( context );
        FirebaseJobDispatcher jobDispatcher = new FirebaseJobDispatcher(googleDriver);

        /* Create the Job to periodically get latest movie */
        Job getLatestMovieJob = jobDispatcher.newJobBuilder()
                /* The Service that will be used to get latest movie */
                .setService(MovieTrailerFirebaseJobService.class)
                /* Set the UNIQUE tag used to identify this Job */
                .setTag(MOVIE_SYNC_TAG)
                /*
                 * Network constraints on which this Job should run. We choose to run on any
                 * network, but you can also choose to run only on un-metered networks or when the
                 * device is charging. It might be a good idea to include a preference for this,
                 * as some users may not want to download any data on their mobile plan. ($$$)
                 */
                .setConstraints(Constraint.ON_ANY_NETWORK)
                /*
                 * setLifetime sets how long this job should persist. The options are to keep the
                 * Job "forever" or to have it die the next time the device boots up.
                 */
                .setLifetime(Lifetime.FOREVER)
                /*
                 * We want to keep get latest movie, so we tell this Job to recur.
                 */
                .setRecurring(true)
                /*
                 * We want the latest movie to be queried every 3 to 4 hours. The first argument for
                 * Trigger's static executionWindow method is the start of the time frame when the
                 * sync should be performed. The second argument is the latest point in time at
                 * which the data should be synced. Please note that this end time is not
                 * guaranteed, but is more of a guideline for FirebaseJobDispatcher to go off of.
                 */
                .setTrigger(Trigger.executionWindow(
                        SYNC_INTERVAL_SECONDS,
                        SYNC_MAX_INTERVAL_SECONDS_WINDOW))
                /*
                 * If a Job with the tag with provided already exists, this new job will replace
                 * the old one.
                 */
                .setReplaceCurrent(true)
                /* Once the Job is ready, call the builder's build method to return the Job */
                .build();

        /* Schedule the Job with the dispatcher */
        jobDispatcher.schedule(getLatestMovieJob);
    }

}
