package com.example.movietrailerexercise.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Person implements Parcelable
{
    private String mName;
    private String mPortraitPath;
    private String mBiography;
    private String mBirthday;
    private String mBirthPlace;
    private String mProfession;
    private String mHomepage;
    private int    mCastID;
    private List<Movie> mMoviesList;
    private List<Movie> mTVsList;

    protected Person(Parcel in) {
        mName = in.readString();
        mPortraitPath = in.readString();
        mBiography = in.readString();
        mBirthday = in.readString();
        mBirthPlace = in.readString();
        mProfession = in.readString();
        mHomepage = in.readString();
        mCastID = in.readInt();
        mMoviesList = in.createTypedArrayList(Movie.CREATOR);
        mTVsList = in.createTypedArrayList(Movie.CREATOR);
    }

    public Person()
    {

    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getPortraitPath() {
        return mPortraitPath;
    }

    public void setPortraitPath(String mPortraitPath) {
        this.mPortraitPath = mPortraitPath;
    }

    public int getID() {
        return mCastID;
    }

    public void setCastID(int mCastID) {
        this.mCastID = mCastID;
    }

    public String getBiography() {
        return mBiography;
    }

    public void setBiography(String mBiography) {
        this.mBiography = mBiography;
    }

    public String getBirthday() {
        return mBirthday;
    }

    public void setBirthday(String mBirthday) {
        this.mBirthday = mBirthday;
    }

    public String getBirthPlace() {
        return mBirthPlace;
    }

    public void setBirthPlace(String mBirthPlace) {
        this.mBirthPlace = mBirthPlace;
    }

    public List<Movie> getMoviesList() {
        return mMoviesList;
    }

    public void setMoviesList(List<Movie> mMoviesList) {
        this.mMoviesList = mMoviesList;
    }

    public List<Movie> getTVsList() {
        return mTVsList;
    }

    public void setTVsList(List<Movie> mTVsList) {
        this.mTVsList = mTVsList;
    }

    public String getProfession() {
        return mProfession;
    }

    public void setProfession(String mProfession) {
        this.mProfession = mProfession;
    }

    public String getHomepage() {
        return mHomepage;
    }

    public void setHomepage(String mHomepage) {
        this.mHomepage = mHomepage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mPortraitPath);
        dest.writeString(mBiography);
        dest.writeString(mBirthday);
        dest.writeString(mBirthPlace);
        dest.writeString(mProfession);
        dest.writeString(mHomepage);
        dest.writeInt(mCastID);
        dest.writeTypedList(mMoviesList);
        dest.writeTypedList(mTVsList);
    }
}
