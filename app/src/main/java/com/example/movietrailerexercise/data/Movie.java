package com.example.movietrailerexercise.data;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Movie implements Parcelable{

    public static final String MOVIE_ID_BUNDLE_KEY = "movie_id";
    public static final String MOVIE_IS_TV_SELECTED_BUNDLE_KEY = "is_tv_selected";

    // common variables for both movie and tv
    private double         movieAverageVote;
    private int            movieID;
    private String         movieTitle;
    private String         movieOverview;
    private String         mHomepage;
    private Uri            mTrailerPath;
    private String         mPosterPath;
    private int            mVoteCount;
    private List<Person>     mActorList;
    private List<Person>     mDirectorList;
    private List<Movie>    mSimilarMovies;
    private List<String>   mGenresList;
    private String         mStatus;
    private int            mRuntime;
    private String         mMediaType;

    // Movie specific variables
    private String         mReleaseDate;
    private List<String>   mSpokenLanguage;
    private int            mBudget;
    private int            mRevenue;

    // TV specific variables
    private String         mFirstAirDate;
    private String         mLastAirDate;
    private String         mNextEpisodeDate;
    private int            mNumberOfEpisodes;
    private int            mNumberOfSeasons;


    public Movie()
    {}


    protected Movie(Parcel in) {
        movieAverageVote = in.readDouble();
        movieID = in.readInt();
        movieTitle = in.readString();
        movieOverview = in.readString();
        mHomepage = in.readString();
        mTrailerPath = in.readParcelable(Uri.class.getClassLoader());
        mPosterPath = in.readString();
        mVoteCount = in.readInt();
        mActorList = in.createTypedArrayList(Person.CREATOR);
        mDirectorList = in.createTypedArrayList(Person.CREATOR);
        mSimilarMovies = in.createTypedArrayList(Movie.CREATOR);
        mGenresList = in.createStringArrayList();
        mStatus = in.readString();
        mRuntime = in.readInt();
        mMediaType = in.readString();
        mReleaseDate = in.readString();
        mSpokenLanguage = in.createStringArrayList();
        mBudget = in.readInt();
        mRevenue = in.readInt();
        mFirstAirDate = in.readString();
        mLastAirDate = in.readString();
        mNextEpisodeDate = in.readString();
        mNumberOfEpisodes = in.readInt();
        mNumberOfSeasons = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(movieAverageVote);
        dest.writeInt(movieID);
        dest.writeString(movieTitle);
        dest.writeString(movieOverview);
        dest.writeString(mHomepage);
        dest.writeParcelable(mTrailerPath, flags);
        dest.writeString(mPosterPath);
        dest.writeInt(mVoteCount);
        dest.writeTypedList(mActorList);
        dest.writeTypedList(mDirectorList);
        dest.writeTypedList(mSimilarMovies);
        dest.writeStringList(mGenresList);
        dest.writeString(mStatus);
        dest.writeInt(mRuntime);
        dest.writeString(mMediaType);
        dest.writeString(mReleaseDate);
        dest.writeStringList(mSpokenLanguage);
        dest.writeInt(mBudget);
        dest.writeInt(mRevenue);
        dest.writeString(mFirstAirDate);
        dest.writeString(mLastAirDate);
        dest.writeString(mNextEpisodeDate);
        dest.writeInt(mNumberOfEpisodes);
        dest.writeInt(mNumberOfSeasons);
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public List<Movie> getSimilarMovies() {
        return mSimilarMovies;
    }

    public void setSimilarMovies(List<Movie> mSimilarMovies) {
        this.mSimilarMovies = mSimilarMovies;
    }

    public List<Person> getActorList()
    {
        return mActorList;
    }

    public void setActorList(List<Person> mActorList) {
        this.mActorList = mActorList;
    }

    public List<String> getGenresList() {
        return mGenresList;
    }

    public void setGenreList(List<String> mGenres) {
        this.mGenresList = mGenres;
    }

    public List<Person> getDirectorList() {
        return mDirectorList;
    }

    public void setDirectorList(List<Person> mDirectorList) {
        this.mDirectorList = mDirectorList;
    }

    public boolean getIsFavourite() {
        return mIsFavourite;
    }

    public void setIsFavourite(boolean mIsFavourite)
    {
        this.mIsFavourite = mIsFavourite;
    }

    private boolean  mIsFavourite;

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String mPosterPath) {
        this.mPosterPath = mPosterPath;
    }

    public int getVoteCount() {
        return mVoteCount;
    }

    public void setVoteCount(int mVoteCount) {
        this.mVoteCount = mVoteCount;
    }

    public String getHomepage() {
        return mHomepage;
    }

    public void setHomepage(String mHomepage) {
        this.mHomepage = mHomepage;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(String mReleaseDate) {
        this.mReleaseDate = mReleaseDate;
    }

    public Uri getTrailerPath() {
        return mTrailerPath;
    }

    public void setTrailerPath(Uri mTrailerPath) {
        this.mTrailerPath = mTrailerPath;
    }

    public String getMovieOverview() {
        return movieOverview;
    }

    public void setMovieOverview(String movieOverview) {
        this.movieOverview = movieOverview;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public double getMovieAverageVote() {
        return movieAverageVote;
    }

    public void setMovieAverageVote(double movieAverageVote) {
        this.movieAverageVote = movieAverageVote;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public int getRuntime() {
        return mRuntime;
    }

    public void setRuntime(int mRuntime) {
        this.mRuntime = mRuntime;
    }

    public int getBudget() {
        return mBudget;
    }

    public void setBudget(int mBudget) {
        this.mBudget = mBudget;
    }

    public int getRevenue() {
        return mRevenue;
    }

    public void setRevenue(int mRevenue) {
        this.mRevenue = mRevenue;
    }

    public String getFirstAirDate() {
        return mFirstAirDate;
    }

    public void setFirstAirDate(String mFirstAirDate) {
        this.mFirstAirDate = mFirstAirDate;
    }

    public String getLastAirDate() {
        return mLastAirDate;
    }

    public void setLastAirDate(String mLastAirDate) {
        this.mLastAirDate = mLastAirDate;
    }

    public String getNextEpisodeDate() {
        return mNextEpisodeDate;
    }

    public void setNextEpisodeDate(String mNextEpisodeDate) {
        this.mNextEpisodeDate = mNextEpisodeDate;
    }

    public int getNumberOfEpisodes() {
        return mNumberOfEpisodes;
    }

    public void setNumberOfEpisodes(int mNumberOfEpisodes) {
        this.mNumberOfEpisodes = mNumberOfEpisodes;
    }

    public int getNumberOfSeasons() {
        return mNumberOfSeasons;
    }

    public void setNumberOfSeasons(int mNumberOfSeasons) {
        this.mNumberOfSeasons = mNumberOfSeasons;
    }

    public List<String> getSpokenLanguage() {
        return mSpokenLanguage;
    }

    public void setSpokenLanguage(List<String> mSpokenLanguage) {
        this.mSpokenLanguage = mSpokenLanguage;
    }

    public String getMediaType() {
        return mMediaType;
    }

    public void setMediaType(String mMediaType) {
        this.mMediaType = mMediaType;
    }
}
