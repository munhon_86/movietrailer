package com.example.movietrailerexercise.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MovieDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "movietrailer.db";

    private static final int DATABASE_VERSION = 1;

    private static final int MAX_NUM_SQL_TABLES = 3;

    private static final String SQL_DELETE_MOVIE_ENTRIES =
            "DROP TABLE IF EXISTS " + MovieContract.MovieEntry.MOVIE_TABLE_NAME;

    private static final String SQL_DELETE_TV_ENTRIES =
            "DROP TABLE IF EXISTS " + MovieContract.MovieEntry.TV_TABLE_NAME;

    private static final String SQL_DELETE_PEOPLE_ENTRIES =
            "DROP TABLE IF EXISTS " + MovieContract.MovieEntry.PEOPLE_TABLE_NAME;

/*    private static final String SQL_DELETE_DIRECTOR_ENTRIES =
            "DROP TABLE IF EXISTS " + MovieContract.MovieEntry.DIRECTOR_TABLE_NAME;*/

    public MovieDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public MovieDbHelper( Context context )
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for ( int i=0; i<MAX_NUM_SQL_TABLES; i++ )
        {
            String tableName = null;
            switch (i) {
                case 0:
                    tableName = MovieContract.MovieEntry.MOVIE_TABLE_NAME;
                    break;
                case 1:
                    tableName = MovieContract.MovieEntry.TV_TABLE_NAME;
                    break;
/*                case 2:
                    tableName = MovieContract.MovieEntry.DIRECTOR_TABLE_NAME;
                    break;*/
                case 2:
                    tableName = MovieContract.MovieEntry.PEOPLE_TABLE_NAME;
                    break;
                default:
                    break;
            }

            if (i <= 1) {
                final String CREATE_SQL_MOVIE_TRAILER_TABLE =
                        "CREATE TABLE IF NOT EXISTS " + tableName + " (" +
                                MovieContract.MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                MovieContract.MovieEntry.COLUMN_MOVIE_ID + " INTEGER NOT NULL, " +
                                MovieContract.MovieEntry.COLUMN_MOVIE_POSTER + " TEXT, " +
                                MovieContract.MovieEntry.COLUMN_MOVIE_TITLE + " TEXT NOT NULL, " +
                                MovieContract.MovieEntry.COLUMN_MOVIE_AVERAGE_VOTE + " REAL NOT NULL DEFAULT " + 0 + "," +
                                "UNIQUE( " + MovieContract.MovieEntry.COLUMN_MOVIE_ID + " ) ON CONFLICT REPLACE);";
                sqLiteDatabase.execSQL(CREATE_SQL_MOVIE_TRAILER_TABLE);
            } else {
                final String CREATE_SQL_MOVIE_TRAILER_TABLE =
                        "CREATE TABLE IF NOT EXISTS " + tableName + " (" +
                                MovieContract.MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                MovieContract.MovieEntry.COLUMN_PERSON_ID + " INTEGER NOT NULL, " +
                                MovieContract.MovieEntry.COLUMN_PERSON_POSTER_PATH + " TEXT, " +
                                MovieContract.MovieEntry.COLUMN_PERSON_NAME + " TEXT NOT NULL, " +
                                "UNIQUE( " + MovieContract.MovieEntry.COLUMN_PERSON_ID + " ) ON CONFLICT REPLACE);";
                sqLiteDatabase.execSQL(CREATE_SQL_MOVIE_TRAILER_TABLE);
            }
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {
        sqLiteDatabase.execSQL(SQL_DELETE_MOVIE_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_TV_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_PEOPLE_ENTRIES);
        onCreate( sqLiteDatabase );
    }
}
