package com.example.movietrailerexercise.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class MovieProvider extends ContentProvider
{
    private MovieDbHelper mMovieDbHelper;
    private static final int ALL_MOVIES_ID = 100;
    private static final int SPECIFIC_MOVIE_ID = 101;

    private static final int ALL_TVS_ID = 200;
    private static final int SPECIFIC_TV_ID = 201;

    //private static final int ALL_DIRECTORS_ID = 300;
    //private static final int SPECIFIC_DRECTOR_ID = 301;

    private static final int ALL_PEOPLE_ID = 400;
    private static final int SPECIFIC_PERSON_ID = 401;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final String TAG = "MovieProvider";

    static
    {
        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.PATH_MOVIES,
                ALL_MOVIES_ID);

        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.PATH_MOVIES + "/#",
                SPECIFIC_MOVIE_ID);

        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.TV_PATH_MOVIES,
                ALL_TVS_ID);

        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.TV_PATH_MOVIES + "/#",
                SPECIFIC_TV_ID);

/*        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.DIRECTOR_PATH,
                ALL_DIRECTORS_ID);

        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.DIRECTOR_PATH + "/#",
                SPECIFIC_DRECTOR_ID);*/

        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.PEOPLE_PATH,
                ALL_PEOPLE_ID);

        sUriMatcher.addURI(MovieContract.CONTENT_AUTHORITY,
                MovieContract.PEOPLE_PATH + "/#",
                SPECIFIC_PERSON_ID);
    }

    @Override
    public boolean onCreate() {
        mMovieDbHelper = new MovieDbHelper( getContext() );
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri,
                        @Nullable String[] projection,
                        @Nullable String selection,
                        @Nullable String[] selectionArgs,
                        @Nullable String sortOrder)
    {

        int matchCode = sUriMatcher.match(uri);
        SQLiteDatabase database = mMovieDbHelper.getReadableDatabase();
        Cursor queryCursor;
        String tableName = getTableName(matchCode);
        switch (matchCode)
        {
            case ALL_MOVIES_ID:
            case ALL_TVS_ID:
            //case ALL_DIRECTORS_ID:
            case ALL_PEOPLE_ID:
                break;

            case SPECIFIC_MOVIE_ID:
            case SPECIFIC_TV_ID:
                selection = MovieContract.MovieEntry.COLUMN_MOVIE_ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId( uri )) };
                break;

            //case SPECIFIC_DRECTOR_ID:
            case SPECIFIC_PERSON_ID:
                selection = MovieContract.MovieEntry.COLUMN_PERSON_ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId( uri )) };
                break;

            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        queryCursor = database.query( tableName,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        queryCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return queryCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ALL_MOVIES_ID:
            case ALL_TVS_ID:
            //case ALL_DIRECTORS_ID:
            case ALL_PEOPLE_ID:
                return MovieContract.MovieEntry.CONTENT_LIST_TYPE;
            case SPECIFIC_MOVIE_ID:
            case SPECIFIC_TV_ID:
            //case SPECIFIC_DRECTOR_ID:
            case SPECIFIC_PERSON_ID:
                return MovieContract.MovieEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {

        final int match = sUriMatcher.match(uri);
        Uri newUri;
        String tableName = getTableName( match );
        switch (match) {
            case ALL_MOVIES_ID:
            case ALL_TVS_ID:
            //case ALL_DIRECTORS_ID:
            case ALL_PEOPLE_ID:
                // Get readable database
                SQLiteDatabase database = mMovieDbHelper.getWritableDatabase();

                long id = database.insert(tableName, null, contentValues);
                if (id == -1) {
                    Log.e(TAG, "Failed to insert row for " + uri);
                    return null;
                }

                // Once we know the ID of the new row in the table,
                // return the new URI with the ID appended to the end of it
                newUri = ContentUris.withAppendedId(uri, id);
                break;
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }

        getContext().getContentResolver().notifyChange( uri, null );
        return newUri;

    }

    @Override
    public int delete(@NonNull Uri uri,
                      @Nullable String selection,
                      @Nullable String[] selectionArgs) {

        // Track the number of rows that were deleted
        int rowsDeleted;

        // Get writeable database
        SQLiteDatabase database = mMovieDbHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        String tableName = getTableName( match );
        switch (match) {
            case ALL_MOVIES_ID:
            case ALL_TVS_ID:
            //case ALL_DIRECTORS_ID:
            case ALL_PEOPLE_ID:
                break;

            case SPECIFIC_MOVIE_ID:
            case SPECIFIC_TV_ID:
                // Delete a single row given by the ID in the URI
                selection = MovieContract.MovieEntry.COLUMN_MOVIE_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                break;

            //case SPECIFIC_DRECTOR_ID:
            case SPECIFIC_PERSON_ID:
                // Delete a single row given by the ID in the URI
                selection = MovieContract.MovieEntry.COLUMN_PERSON_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                break;

            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }

        rowsDeleted = database.delete(tableName, selection, selectionArgs);

        // If 1 or more rows were deleted, then notify all listeners that the data at the
        // given URI has changed
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs){
        final int match = sUriMatcher.match(uri);
        String tableName = getTableName( match );
        switch (match) {
            case ALL_MOVIES_ID:
            case ALL_TVS_ID:
            //case ALL_DIRECTORS_ID:
            case ALL_PEOPLE_ID:
                break;

            case SPECIFIC_MOVIE_ID:
            case SPECIFIC_TV_ID:
                selection = MovieContract.MovieEntry.COLUMN_MOVIE_ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                break;

            //case SPECIFIC_DRECTOR_ID:
            case SPECIFIC_PERSON_ID:
                selection = MovieContract.MovieEntry.COLUMN_PERSON_ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                break;

            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }

        return updateTable(uri, tableName, contentValues, selection, selectionArgs);
    }

    private int updateTable(Uri uri,
                            String tableName,
                            ContentValues contentValues,
                            String selection,
                            String[] selectionArgs)
    {
        SQLiteDatabase database = mMovieDbHelper.getWritableDatabase();
        int numRows = database.update(tableName, contentValues, selection, selectionArgs);
        if (numRows != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numRows;
    }

    private String getTableName( int matchCode )
    {
       if  (matchCode == ALL_MOVIES_ID || matchCode == SPECIFIC_MOVIE_ID) {
           return MovieContract.MovieEntry.MOVIE_TABLE_NAME;
       }
       else if (matchCode == ALL_TVS_ID || matchCode == SPECIFIC_TV_ID) {
           return MovieContract.MovieEntry.TV_TABLE_NAME;
       }
/*       else if (matchCode == ALL_DIRECTORS_ID || matchCode == SPECIFIC_DRECTOR_ID) {
           return MovieContract.MovieEntry.DIRECTOR_TABLE_NAME;
       }*/
       else {
           return MovieContract.MovieEntry.PEOPLE_TABLE_NAME;
       }
    }

}
