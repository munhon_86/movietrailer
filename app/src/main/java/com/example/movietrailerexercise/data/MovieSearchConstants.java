package com.example.movietrailerexercise.data;

import android.net.Uri;

public final class MovieSearchConstants {

    private static final int CURRENT_MOVIE_DB_VERSION = 3;

    // The following are the constants that will not be changed
    private static final String MOVIE_DB_DOMAIN = "https://api.themoviedb.org";
    public static final String MOVIE_DB_POSTER_DOMAIN = "https://image.tmdb.org/t/p/w500";
    public static final String MOVIE_DB_HIGH_QUALITY_POSTER_DOMAIN =
            "https://image.tmdb.org/t/p/w500";
    public static final String YOUTUBE_MOVIE_TRAILER_WEBSITE = "https://www.youtube.com/watch";
    public static final String MOVIE_DB_PERSON_DETAIL_QUERY_BASE = "https://api.themoviedb.org/3/person";

    public static final String API_KEY_VALUE = "07bf35232c37a82faa5122fdf89c6972";

    // movie query search
    public static final String POPULAR_SEARCH = "popular";
    public static final String UPCOMING_SEARCH = "upcoming";
    public static final String TOP_RATED_SEARCH = "top_rated";
    public static final String NOW_PLAYING_SEARCH = "now_playing";

    // tv query search
    public static final String TV_POPULAR_SEARCH = "popular";
    public static final String TV_UPCOMING_SEARCH = "on_the_air";
    public static final String TV_TOP_RATED_SEARCH = "top_rated";
    public static final String TV_NOW_PLAYING_SEARCH = "airing_today";

    // general ( common ) search parameters/keys
    public static final String API_KEY = "api_key";
    public static final String APPEND_TO_RESPONSE = "append_to_response";
    public static final String LANGUAGE_QUERY_SEARCH = "language";
    public static final String REGION_QUERY_SEARCH = "region";
    public static final String LATEST_QUERY_SEARCH = "latest";
    public static final String CREDITS_QUERY_SEARCH = "credits";
    public static final String VIDEO_QUERY_SEARCH = "videos";
    public static final String PAGE_QUERY_SEARCH = "page";
    public static final String TOTAL_PAGE_QUERY_SEARCH = "total_pages";
    public static final String USER_QUERY_KEY = "query";

    // The following are the JSON string keys to obtain the data
    public static final String JSON_MOVIE_TITLE_KEY = "title";
    public static final String JSON_GENRE_IDS_KEY = "genre_ids";
    public static final String JSON_POSTER_PATH_KEY = "poster_path";
    public static final String JSON_VOTE_COUNT_KEY = "vote_count";
    public static final String JSON_MOVIE_ID_KEY = "id";
    public static final String JSON_VOTE_AVERAGE_KEY = "vote_average";
    public static final String JSON_POPULARITY_KEY = "popularity";
    public static final String JSON_OVERVIEW_KEY = "overview";
    public static final String JSON_ARRAY_RESULTS_KEY = "results";
    public static final String JSON_ARRAY_CASTS_KEY = "cast";
    public static final String JSON_ARRAY_CREW_KEY = "crew";
    public static final String JSON_COMMON_NAME_KEY = "name";
    public static final String JSON_MOVIE_DETAIL_GENRE_NAME_KEY = "name";
    public static final String JSON_CREW_JOB_KEY = "job";
    public static final String JSON_COMMON_HOMEPAGE_KEY = "homepage";
    public static final String JSON_DETAIL_MOVIE_GENRES_ARRAY_KEY = "genres";
    public static final String JSON_QUERY_STATUS_CODE = "status_code";
    public static final String JSON_VIDEO_YOUTUBE_KEY = "key";
    public static final String JSON_VIDEOS_RESULT_KEY = "videos";
    public static final String JSON_CREDITS_RESULT_KEY = "credits";
    public static final String JSON_STATUS_RESULT_KEY = "status";

    // movie specific keys
    public static final String JSON_SPOKEN_LANGUAGE_RESULT_KEY = "spoken_languages";
    public static final String JSON_REVENUE_RESULT_KEY = "revenue";
    public static final String JSON_BUDGET_RESULT_KEY = "budget";
    public static final String JSON_RELEASE_DATE_KEY = "release_date";
    public static final String JSON_RUNTIME_RESULT_KEY = "runtime";

    // tv specific keys
    public static final String JSON_FIRST_AIR_DATE_KEY = "first_air_date";
    public static final String JSON_NUMBER_OF_EPISODES_KEY = "number_of_episodes";
    public static final String JSON_NUMBER_OF_SEASONS_KEY = "number_of_seasons";
    public static final String JSON_TV_TITLE_KEY = "name";
    public static final String JSON_TV_LAST_AIR_DATE_KEY = "last_air_date";
    public static final String JSON_TV_RUNTIME_RESULT_KEY = "episode_run_time";

    // casts detail results key
    public static final String JSON_CASTS_PORTRAIT_PATH_KEY = "profile_path";
    public static final String JSON_CASTS_ID_KEY = "cast_id";
    public static final String JSON_COMMON_ID_KEY = "id";
    public static final String JSON_MOVIES_SIMILAR_ID_KEY = "similar";
    public static final String JSON_CASTS_BIRTHDAY_KEY = "birthday";
    public static final String JSON_CASTS_JOB_PROFESSION_KEY =  "known_for_department";
    public static final String JSON_CASTS_BIOGRAPHY_KEY = "biography";
    public static final String JSON_CASTS_PLACE_OF_BIRTH_KEY = "place_of_birth";
    public static final String JSON_CASTS_MOVIE_CREDITS_KEY = "movie_credits";
    public static final String JSON_CASTS_TV_CREDITS_KEY = "tv_credits";

    // search query keys
    public static final String JSON_SEARCH_MEDIA_TYPE_KEY = "media_type";
    public static final String MEDIA_TYPE_PERSON = "person";
    public static final String MEDIA_TYPE_MOVIE = "movie";
    public static final String MEDIA_TYPE_TV = "tv";

    public static final String DIRECTOR_JOB = "director";

    public static final Uri MOVIE_SEARCH_BASE_URI =
            Uri.parse( MOVIE_DB_DOMAIN ).buildUpon()
                    .appendPath(Integer.toString(CURRENT_MOVIE_DB_VERSION))
                    .appendPath("movie")
                    .build();

    public static final Uri TV_SEARCH_BASE_URI =
            Uri.parse( MOVIE_DB_DOMAIN ).buildUpon()
                    .appendPath(Integer.toString(CURRENT_MOVIE_DB_VERSION))
                    .appendPath("tv")
                    .build();

    public static final Uri USER_SEARCH_BASE_URI =
            Uri.parse(MOVIE_DB_DOMAIN).buildUpon()
                .appendPath(Integer.toString(CURRENT_MOVIE_DB_VERSION))
                .appendPath("search")
                .appendPath("multi")
            .build();

    public static final String MOVIE_DETAILS_APPENDED_QUERY_SEARCH =
            CREDITS_QUERY_SEARCH + "," + VIDEO_QUERY_SEARCH + "," + JSON_MOVIES_SIMILAR_ID_KEY;

    public static final String CAST_DETAIlS_APPENDED_QUERY_SEARCH =
            JSON_CASTS_MOVIE_CREDITS_KEY + "," + JSON_CASTS_TV_CREDITS_KEY;


    private static boolean sIsTVSelected;

    public static boolean getIsTVSelected() {
        return sIsTVSelected;
    }

    public static void setIsTVSelected(boolean sIsTVSelected) {
        MovieSearchConstants.sIsTVSelected = sIsTVSelected;
    }
}
