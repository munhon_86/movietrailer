package com.example.movietrailerexercise.data;

import java.util.List;

public class QueryResponse {
    private int         mTotalPage;
    private List<Movie> mMovieList;
    private List<Movie> mTVList;
    private List<Person> mPersonList;

    public int getTotalPage() {
        return mTotalPage;
    }

    public void setTotalPage(int mTotalPage) {
        this.mTotalPage = mTotalPage;
    }

    public List<Movie> getMovieList() {
        return mMovieList;
    }

    public void setMovieList(List<Movie> mMovieList) {
        this.mMovieList = mMovieList;
    }

    public List<Movie> getTVList() {
        return mTVList;
    }

    public void setTVList(List<Movie> mTVList) {
        this.mTVList = mTVList;
    }

    public List<Person> getCastList() {
        return mPersonList;
    }

    public void setCastList(List<Person> mPersonList) {
        this.mPersonList = mPersonList;
    }
}
