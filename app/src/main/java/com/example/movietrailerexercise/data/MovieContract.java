package com.example.movietrailerexercise.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class MovieContract
{
    public static final String CONTENT_AUTHORITY = "com.example.movietrailerexercise";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_MOVIES = "movies";

    public static final String TV_PATH_MOVIES = "tvs";

    public static final String PEOPLE_PATH = "people";

    //public static final String DIRECTOR_PATH = "director";

    public static final class MovieEntry implements BaseColumns
    {
        public static final Uri MOVIE_CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_MOVIES)
                .build();

        public static final Uri TV_CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TV_PATH_MOVIES)
                .build();

/*        public static final Uri DIRECTOR_CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(DIRECTOR_PATH)
                .build();*/

        public static final Uri PEOPLE_CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PEOPLE_PATH)
                .build();

        public static final String MOVIE_TABLE_NAME = "movies";

        public static final String TV_TABLE_NAME = "tvs";

        public static final String PEOPLE_TABLE_NAME = "people";

        //public static final String CAST_TABLE_NAME = "cast";

        //public static final String DIRECTOR_TABLE_NAME = "director";

        public static final String COLUMN_MOVIE_ID = "movie_id";

        public static final String COLUMN_MOVIE_POSTER = "movie_poster";

        public static final String COLUMN_MOVIE_TITLE = "movie_title";

        public static final String COLUMN_MOVIE_AVERAGE_VOTE = "movie_average_vote";

        public static final String COLUMN_PERSON_ID = "person_id";

        public static final String COLUMN_PERSON_POSTER_PATH = "person_poster";

        public static final String COLUMN_PERSON_NAME = "person_name";

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIES;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOVIES;
    }
}
