package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.movietrailerexercise.data.QueryResponse;
import com.example.movietrailerexercise.utilities.JsonUtil;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.io.IOException;
import java.net.URL;

public class QueryDataLoader extends android.support.v4.content.AsyncTaskLoader<QueryResponse> {
    private static final String LOG_TAG = QueryDataLoader.class.getSimpleName();
    private URL mSearchUrl;
    //private Context mContext;
    private Boolean mIsTVSelected;

    public QueryDataLoader(Context context,
                           URL url,
                           @Nullable Boolean isTVSelected)
    {
        super(context);
        //mContext = context;
        mIsTVSelected = isTVSelected;
        mSearchUrl = url;
        Log.v(LOG_TAG, "Loader created ");
    }

    @Override
    public QueryResponse loadInBackground() {
        Log.v(LOG_TAG, "Loading in background ");
        if (mSearchUrl == null) {
            return null;
        }

        String searchResponse;
        try {
            searchResponse = QueryUtil.getResponseFromHttpUrl(mSearchUrl);
        } catch (IOException e)
        {
            return null;
        }

        if (mIsTVSelected == null) {
            return JsonUtil.extractUserSearchResults(searchResponse);
        } else {
            return JsonUtil.extractSimpleMovieDataList(searchResponse, mIsTVSelected);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    public void updateQueryUrl(URL searchURL) {
        mSearchUrl = searchURL;
    }
}
