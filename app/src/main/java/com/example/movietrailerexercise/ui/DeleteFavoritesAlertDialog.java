package com.example.movietrailerexercise.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.example.movietrailerexercise.R;

public class DeleteFavoritesAlertDialog extends DialogFragment
{
    public interface DeleteMovieListener
    {
        public void onConfirmDelete();
    }

    // Use this instance of the interface to deliver action events
    private DeleteMovieListener mListener;
    private boolean mDeleteAllMovies;

    public void setCallbackListener(DeleteMovieListener listener)
    {
        mListener = listener;
    }
    public void setDeleteAllMovies( boolean deleteAllMovies )
    {
        mDeleteAllMovies = deleteAllMovies;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
        if ( mDeleteAllMovies )
        {
            builder.setMessage( getString(R.string.delete_all_movies_alert_message));
        }
        else
        {
            builder.setMessage( getString(R.string.delete_favorite_alert_dialog_message));
        }

        builder.setNegativeButton(getString(R.string.delete_cancel_button),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {

                    }
                })
                .setPositiveButton(getString(R.string.delete_confirm_button),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        mListener.onConfirmDelete();
                    }
                });


        return builder.create();
    }

/*    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DeleteMovieListener) getActivity();
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(getActivity().getClass().toString()
                    + " must implement NoticeDialogListener");
        }
    }*/
}
