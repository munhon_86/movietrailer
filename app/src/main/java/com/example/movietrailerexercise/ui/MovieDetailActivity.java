package com.example.movietrailerexercise.ui;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movietrailerexercise.MainActivity;
import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Person;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.databinding.ActivityDetailBinding;
import com.example.movietrailerexercise.utilities.PreferenceUtil;
import com.example.movietrailerexercise.utilities.ProviderUtil;
import com.example.movietrailerexercise.utilities.QueryUtil;
//import com.nostra13.universalimageloader.core.ImageLoader;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MovieDetailActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Movie>,
        SharedPreferences.OnSharedPreferenceChangeListener
{

    private static final String TAG = "MovieDetailActivity";

    private ActivityDetailBinding mBinding;

    private static final int INVALID_MOVIE_ID = -1;

    private Movie mMovieData;
    private String mLanguage;
    private int mMovieID;
    private boolean mIsTVSelected;

    private static final int DETAIL_ACTIVITY_LOADER_ID = 100;

    private View mLayout;
    private RecyclerView mCastsRecyclerView;
    private RecyclerView mDirectorRecyclerView;
    private TextView mCastNotAvailableTextView;
    private TextView mDirectorNotAvailableTextView;

    public enum LoaderOption
    {
        LOADER_INIT,
        LOADER_RESTART,
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Log.v(TAG, "onCreate: ");

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        mCastsRecyclerView = findViewById(R.id.casts_name_recycler_view);
        mDirectorRecyclerView = findViewById(R.id.director_name_recycler_view);
        mCastNotAvailableTextView = findViewById(R.id.casts_name_not_available_textview);
        mDirectorNotAvailableTextView = findViewById(R.id.director_name_not_available_textview);

        mLayout = findViewById(R.id.detail_movie_info);

        Intent intent = getIntent();
        mMovieID = intent.getIntExtra(Movie.MOVIE_ID_BUNDLE_KEY, INVALID_MOVIE_ID);
        mIsTVSelected =
                intent.getBooleanExtra(Movie.MOVIE_IS_TV_SELECTED_BUNDLE_KEY, false );

        SharedPreferences shrePref = PreferenceUtil.getMovieFavoritesSharedPreference(
                mIsTVSelected,
                this);

        String movieIDsFavListKey = PreferenceUtil.getMovieFavoritesListKey(mIsTVSelected, this);

        Set<String> movieIDsFavoriteList = shrePref.getStringSet(
                movieIDsFavListKey,
                new HashSet<String>() );

        // check if the current movie (or tv) id is part of the favorites list
        boolean isFavorite = false;
        for (String id : movieIDsFavoriteList) {
            if (Integer.parseInt( id ) == mMovieID) {
                isFavorite = true;
                break;
            }
        }
        if (isFavorite) {
            mBinding.detailMovieInfo.favoritesCheckbox.setChecked( true );
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( this );
        prefs.registerOnSharedPreferenceChangeListener( this );

        initOrRestartLoader( LoaderOption.LOADER_RESTART );
    }

    private void startNewFragmentView(int LayoutID, Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(LayoutID, fragment);
        fragmentTransaction.commit();
    }

    private void updateMovieDetailViews()
    {
        // update movie poster
        updateMoviePoster();

        // set movie title
        String movieTitle = mMovieData.getMovieTitle();
        mBinding.detailMovieInfo.activityDetailMovieTitle.setText(movieTitle);

        // update movie overview
        updateMovieOverview();

        // update general info view
        updateGeneralInfoView();

        // update both directors and casts recycler views
        updateCreditsView();

        // update similar movies view
        updateMovieSimilarView();

    }

    private void updateMovieOverview() {
        TextView overviewLabel = findViewById(R.id.generic_title_label_textview);
        TextView overviewText = findViewById(R.id.generic_text_textview);

        overviewLabel.setText(getString(R.string.overview_label));

        String overview = mMovieData.getMovieOverview();
        if (overview != null && !overview.equals("null") && overview.length() > 0) {
            overviewText.setText(overview);
        } else {
            overviewText.setText(getString(R.string.not_available_text_message));
        }
    }


    private void updateMoviePoster() {
        String posterPath = mMovieData.getPosterPath();
        if (posterPath == null || posterPath.length() == 0) {
            mBinding.detailMovieInfo.activityDetailMoviePoster.setImageResource(R.mipmap.ic_launcher);
        }
        else {
            Glide.with(this)
                    .load(posterPath)
                    .into(mBinding.detailMovieInfo.activityDetailMoviePoster);
        }
    }

    private void updateMovieSimilarView() {
        List<Movie> similarMovies = mMovieData.getSimilarMovies();
        if ( similarMovies != null ) {
            MoviesListHorizontalViewFragment fragment =
                    MoviesListHorizontalViewFragment.newInstance(similarMovies, mIsTVSelected);
            startNewFragmentView(R.id.similar_movies_fragment_container, fragment);
        }
    }

    private void updateCreditsView() {
        LinearLayoutManager actorListlayoutManager =
                new LinearLayoutManager(
                        this,
                        LinearLayoutManager.HORIZONTAL,
                        false);

        LinearLayoutManager directorListLayoutManager =
                new LinearLayoutManager(
                        this,
                        LinearLayoutManager.HORIZONTAL,
                        false);

        List<Person> actorList = mMovieData.getActorList();

        if ( actorList != null && actorList.size() > 0 )
        {
            CastsListRecyclerViewAdapter actorsListAdapter =
                    new CastsListRecyclerViewAdapter(this, actorList);
            mCastsRecyclerView.setLayoutManager(actorListlayoutManager);
            mCastsRecyclerView.setAdapter(actorsListAdapter);
            mCastsRecyclerView.setHasFixedSize(true);
            mCastsRecyclerView.setVisibility(View.VISIBLE);
            mCastNotAvailableTextView.setVisibility(View.GONE);
        }
        else
        {
            mCastsRecyclerView.setVisibility(View.GONE);
            mCastNotAvailableTextView.setText( getString(R.string.not_available_text_message) );
            mCastNotAvailableTextView.setVisibility(View.VISIBLE);
        }

        List<Person> directorList = mMovieData.getDirectorList();
        if ( directorList != null && directorList.size() > 0 )
        {
            CastsListRecyclerViewAdapter directorsListAdapter =
                    new CastsListRecyclerViewAdapter(this, directorList);
            mDirectorRecyclerView.setLayoutManager( directorListLayoutManager );
            mDirectorRecyclerView.setAdapter( directorsListAdapter );
            mDirectorRecyclerView.setHasFixedSize( true );
            mDirectorRecyclerView.setVisibility(View.VISIBLE);
            mDirectorNotAvailableTextView.setVisibility(View.GONE);
        }
        else
        {
            mDirectorRecyclerView.setVisibility(View.GONE);
            mDirectorNotAvailableTextView.setText( getString(R.string.not_available_text_message) );
            mDirectorNotAvailableTextView.setVisibility(View.VISIBLE);
        }
    }

    private void updateGeneralInfoView() {
        MovieGeneralInfoFragment generalInfoFragment =
                MovieGeneralInfoFragment.newInstance(mIsTVSelected, mMovieData);
        startNewFragmentView(R.id.movie_detail_general_info_container, generalInfoFragment);
    }

    private void setupButtons() {
        final Uri movieTrailer = mMovieData.getTrailerPath();
        if (movieTrailer == null) {
            mBinding.detailMovieInfo.playButton.setEnabled(false);

        } else {
            mBinding.detailMovieInfo.playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Create a new intent to view the trailer URI
                    Intent websiteIntent = new Intent(Intent.ACTION_VIEW, movieTrailer);
                    websiteIntent.putExtra("force_fullscreen", true);
                    // Send the intent to launch a new activity
                    startActivity(websiteIntent);
                }
            });
        }

        mBinding.detailMovieInfo.favoritesCheckbox.setOnCheckedChangeListener
                (new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if ( isChecked )
                {
                    // save into database
                    saveMovie();
                }
                else
                {
                    removeMovieFromFavorites();
                }
            }
        });
    }

    private void removeMovieFromFavorites()
    {
        int rowsDeleted = ProviderUtil.deleteMovie( this, mMovieID, mIsTVSelected );
        String toastMessage;
        if ( rowsDeleted > 0 )
        {
            toastMessage = getString(R.string.movie_deleted_from_favorites_success,
                    mMovieData.getMovieTitle());
        }
        else
        {
            toastMessage = getString(R.string.movie_deleted_from_favorites_fail);
        }
        Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
    }

    private void saveMovie()
    {
        Uri newUri = ProviderUtil.saveMovieToDatabase( this, mMovieData, mIsTVSelected );
        if ( newUri != null ) {
            Toast.makeText(this,
                    getString(R.string.movie_added_to_favorites, mMovieData.getMovieTitle()),
                    Toast.LENGTH_SHORT)
                    .show();
        }
        else
        {
            Toast.makeText(this,
                    "Failed to save into database",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if (id == R.id.home_icon) {
            startActivity( new Intent(this, MainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public Loader<Movie> onCreateLoader(int loaderID, Bundle bundle)
    {
        URL queryURL = null;
        try {
            queryURL = QueryUtil.buildMovieDetailQuery( mMovieID, mLanguage, mIsTVSelected);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.v(TAG, "onCreateLoader URI: " + queryURL.toString() );

        return new MovieDetailDataLoader( this, queryURL, mIsTVSelected );
    }

    @Override
    public void onLoadFinished(Loader<Movie> loader, Movie movieData)
    {
        //Log.v(TAG, "onLoadFinished : " + movieData.getMovieID() );
        if ( movieData == null )
        {
            mBinding.emptyDataTextview
                    .setText( getString(R.string.failed_to_load_detail_movie_data));
            showErrorMessage();
        }
        else
        {
            mMovieData = movieData;
            updateMovieDetailViews();
            setupButtons();
            showLoadedData();
        }
    }

    @Override
    public void onLoaderReset(Loader<Movie> loader)
    {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String prefKey)
    {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( this );
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    private void showLoading()
    {
        mLayout.setVisibility(View.GONE);
        mBinding.emptyDataTextview.setVisibility(View.GONE);
        mBinding.loadingSpinner.setVisibility(View.VISIBLE);
    }

    private void showLoadedData()
    {
        mBinding.emptyDataTextview.setVisibility(View.GONE);
        mBinding.loadingSpinner.setVisibility(View.GONE);
        mLayout.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage()
    {
        mLayout.setVisibility(View.INVISIBLE);
        mBinding.loadingSpinner.setVisibility(View.GONE);
        mBinding.emptyDataTextview.setVisibility(View.VISIBLE);
    }

    private void initOrRestartLoader( LoaderOption option )
    {
        showLoading();

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        if ( isConnected )
        {
            getLanguagePreference();
            switch ( option )
            {
                case LOADER_INIT:
                    getLoaderManager().initLoader
                            (DETAIL_ACTIVITY_LOADER_ID, null, this );
                    break;
                case LOADER_RESTART:
                    getLoaderManager().restartLoader
                            (DETAIL_ACTIVITY_LOADER_ID, null, this );
                    break;
                default:
                    break;
            }
        }
        else
        {
            mBinding.emptyDataTextview.setText( getString(R.string.no_internet_connection));
            showErrorMessage();
        }
    }

    private void getLanguagePreference() {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);

        // get the language option
        mLanguage = sharedPreferences.getString( getString(R.string.pref_general_language_key),
                getString(R.string.pref_general_language_default));
    }

}
