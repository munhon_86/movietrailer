package com.example.movietrailerexercise.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.utilities.ViewUtil;
//import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class MovieRecyclerViewAdapter extends RecyclerView.Adapter<MovieRecyclerViewAdapter.MovieAdapterViewHolder> {
    private static final String TAG = "MovieRecyclerViewAdapter";

    /* The context we use to utility methods, app resources and layout inflaters */
    private final Context mContext;

    private List<Bitmap> mMoviePosterList;
    private List<Movie> mMovieList;
    private boolean mResizeView;
    private boolean mIsTVSelected;

    public MovieRecyclerViewAdapter(@NonNull Context context,
                                    boolean isTVSelected,
                                    boolean resizeView) {
        mContext = context;
        mIsTVSelected = isTVSelected;
        mResizeView = resizeView;
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param parent    The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (like ours does) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new MovieAdapterViewHolder that holds the View for each list item
     */
    @Override
    public MovieRecyclerViewAdapter.MovieAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(mContext).inflate(R.layout.movietabview, parent, false);
        View view = LayoutInflater.from(mContext).inflate(R.layout.movie_image_tab, parent, false);
        view.setFocusable(true);

        return new MovieAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieRecyclerViewAdapter.MovieAdapterViewHolder holder, final int position) {

        Movie movie = mMovieList.get(position);
        String movieUrl = movie.getPosterPath();

        //ImageLoader.getInstance().displayImage(movieUrl, holder.mMovieImage);
        Glide.with(mContext).load(movieUrl).into(holder.mMovieImage);

        holder.mMovieTitle.setText( movie.getMovieTitle() );

        double movieAverageVote = movie.getMovieAverageVote();
        ViewUtil.updateTextView( holder.mVoteAverage, movieAverageVote, mContext );
    }

    @Override
    public int getItemCount() {
        if ( mMovieList == null )
        {
            return 0;
        }
        return mMovieList.size();
    }

    public class MovieAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ImageView mMovieImage;
        final TextView mMovieTitle;
        final TextView mVoteAverage;
        //final ProgressBar mMovieImageProgressBar;

        MovieAdapterViewHolder(View view) {
            super(view);
            mMovieImage = view.findViewById(R.id.thumbnail);
            mMovieTitle = view.findViewById(R.id.movie_title);
            mVoteAverage = view.findViewById(R.id.movie_vote_average_textview);
            //mMovieImageProgressBar = view.findViewById(R.id.image_view_progress_bar);
            view.setOnClickListener(this);
            if (mResizeView)
            {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ( (Activity) mContext ).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                // This code is used to get the screen dimensions of the user's device
                int width = displayMetrics.widthPixels;
                //int height = displayMetrics.heightPixels;

                // Set the ViewHolder width to be a third of the screen size, and height to wrap content
                itemView.setLayoutParams(new RecyclerView.LayoutParams(
                        (int) Math.round(width * 0.85),
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));

            }
        }

        /**
         * This gets called by the child views during a click. We fetch the date that has been
         * selected, and then call the onClick handler registered with this adapter, passing that
         * position.
         *
         * @param view the View that was clicked
         */
        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Movie movieData = mMovieList.get(adapterPosition);
            Intent startMovieDetailIntent = new Intent(mContext, MovieDetailActivity.class );
            startMovieDetailIntent.putExtra(Movie.MOVIE_ID_BUNDLE_KEY, movieData.getMovieID());
            startMovieDetailIntent.putExtra(Movie.MOVIE_IS_TV_SELECTED_BUNDLE_KEY, mIsTVSelected);
            mContext.startActivity(startMovieDetailIntent);
        }
    }

    public void appendDataList(List<Movie> movieList) {

        if (mMovieList != null && movieList != null) {
            mMovieList.addAll(movieList);
            notifyDataSetChanged();
        }
    }

    public void updateDataList(List<Movie> movieList) {
        if (movieList != null) {
            mMovieList = movieList;
            notifyDataSetChanged();
        }
    }

    public void resetData() {
        mMovieList = null;
        notifyDataSetChanged();
    }

    public boolean hasValidData()
    {
        /*return ( mMovieList != null && mMoviePosterList != null );*/
        return mMovieList != null;
    }

}
