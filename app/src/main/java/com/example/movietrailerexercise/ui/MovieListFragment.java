package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.data.QueryResponse;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.net.MalformedURLException;
import java.net.URL;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static android.view.View.GONE;


public class MovieListFragment extends android.support.v4.app.Fragment implements
        android.support.v4.app.LoaderManager.LoaderCallbacks<QueryResponse>,
        SharedPreferences.OnSharedPreferenceChangeListener
{

    private RecyclerView mRecyclerView;
    private MovieRecyclerViewAdapter mMovieRecyclerViewAdapter;
    private ProgressBar mProgressBar;
    private TextView mEmptyTextView;
    private SwipeRefreshLayout mRefreshLayout;
    private FloatingActionButton mFloatingActionBtn;

    private Context mContext;

    private static final String TAG = "NowPlayingMovieList";

    private String mLanguage;
    private String mRegion;
    private int mLoaderID;
    private boolean mIsTVSelected;
    private int mCurrPage;

    private static final String ARGUMENTS_POSITION_KEY = "position";

    public enum LoaderOption
    {
        LOADER_INIT,
        LOADER_RESTART,
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String prefKey)
    {
        Log.v(TAG, "onSharedPreferenceChanged: ");
        if (prefKey.equals(getString(R.string.pref_general_language_key)) ||
            prefKey.equals(getString(R.string.pref_general_region_key)) ) {
            initOrRestartLoader( LoaderOption.LOADER_RESTART );
        }
    }

    public static MovieListFragment newInstance (int position, boolean isTVSelected )
    {
        Bundle args = new Bundle();
        MovieListFragment movieListFragment = new MovieListFragment();
        args.putInt(ARGUMENTS_POSITION_KEY, position);
        movieListFragment.setArguments(args);
        return movieListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        mRecyclerView = view.findViewById(R.id.fragment_movielist_recycler_view);
        mProgressBar = view.findViewById(R.id.loading_spinner);
        mEmptyTextView = view.findViewById(R.id.empty_data_textview);
        mRefreshLayout = view.findViewById(R.id.swiperefresh);
        mFloatingActionBtn = view.findViewById(R.id.top_floating_action_btn);
        mFloatingActionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        mContext = getContext();
        //mImageLoader = ImageLoader.getInstance();
        Log.d(TAG, "onCreateView: ");

        mLoaderID = getArguments().getInt(ARGUMENTS_POSITION_KEY);

        SharedPreferences sharedPreferences =
                mContext.getSharedPreferences(getString(R.string.movie_tv_preference),
                        Context.MODE_PRIVATE);
        mIsTVSelected = sharedPreferences.getBoolean(
                getString(R.string.switch_to_tv_pref_key),
                getResources().getBoolean(R.bool.switch_to_tv_tabs_default));

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                mCurrPage = 1;
                initOrRestartLoader(LoaderOption.LOADER_RESTART);
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( mContext );
        prefs.registerOnSharedPreferenceChangeListener( this );

        Log.v(TAG, "onCreateView: ");

        GridLayoutManager layoutManager =
                new GridLayoutManager( mContext, 2 );

/*        LinearLayoutManager layoutManager = new LinearLayoutManager(
                mContext,
                LinearLayoutManager.VERTICAL,
                false);*/

        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.setItemViewCacheSize(12);

        mMovieRecyclerViewAdapter = new MovieRecyclerViewAdapter(mContext,
                mIsTVSelected,
                false);

        mRecyclerView.setAdapter(mMovieRecyclerViewAdapter);

        mCurrPage = 1;
        mMovieRecyclerViewAdapter.resetData();
        initOrRestartLoader(LoaderOption.LOADER_RESTART);

        return view;
    }

    @Override
    public android.support.v4.content.Loader<QueryResponse> onCreateLoader(int loaderID, Bundle args) {

        URL searchURL = null;
        try {
            searchURL = QueryUtil.buildSearchQuery(
                    loaderID,
                    mRegion,
                    mLanguage,
                    mIsTVSelected,
                    mCurrPage);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Log.v(TAG, "onCreateLoader URI: " + searchURL.toString() );

        return new QueryDataLoader(getContext(), searchURL, mIsTVSelected);
    }

    @Override
    public void onLoadFinished(final android.support.v4.content.Loader<QueryResponse> loader,
                               QueryResponse movieTVListQueryResponse)
    {
        if (movieTVListQueryResponse == null)
        {
            Log.v(TAG, "onLoadFinished: movieTVListQueryResponse is NULL!");
            if ( mRefreshLayout.isRefreshing() )
            {
                mRefreshLayout.setRefreshing(false);
                if ( mMovieRecyclerViewAdapter.hasValidData() )
                {
                    Toast.makeText( mContext,
                            getString(R.string.failed_to_refresh_movie_list),
                            Toast.LENGTH_SHORT ).show();
                }
                else
                {
                    mEmptyTextView.setText( getString(R.string.no_movies_found));
                    showErrorMessage();
                }
            }
            else if ( !mMovieRecyclerViewAdapter.hasValidData() ) {
                mEmptyTextView.setText( getString(R.string.no_movies_found));
                showErrorMessage();
            }
        } else {
            showData();
            if (mCurrPage == 1) {
                mMovieRecyclerViewAdapter.updateDataList(movieTVListQueryResponse.getMovieList());
            } else {
                mMovieRecyclerViewAdapter.appendDataList(movieTVListQueryResponse.getMovieList());
            }

            int totalPage = movieTVListQueryResponse.getTotalPage();
            Log.v(TAG, "Total Page for position " + mLoaderID + ": " +totalPage);
            if (mCurrPage < totalPage) {
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        mCurrPage++;
                        try {
                            URL searchURL = QueryUtil.buildSearchQuery(
                                    mLoaderID,
                                    mRegion,
                                    mLanguage,
                                    mIsTVSelected,
                                    mCurrPage);
                            ((QueryDataLoader) loader).updateQueryUrl(searchURL);
                            loader.forceLoad();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        //restartLoader();
                    }
                }.start();
            }
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<QueryResponse> loader) {
        mMovieRecyclerViewAdapter.resetData();
        Log.v(TAG, "onLoaderReset: ");
    }

    private void restartLoader() {
        performLoaderAction(LoaderOption.LOADER_RESTART);
    }

    private void getLanguageAndRegionOption()
    {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(mContext);

        // get the region option
        mRegion = sharedPreferences.getString( getString(R.string.pref_general_region_key),
                                                getString(R.string.pref_general_region_default));

        // get the language option
        mLanguage = sharedPreferences.getString( getString(R.string.pref_general_language_key),
                                                getString(R.string.pref_general_language_default));
    }

    private void initOrRestartLoader( LoaderOption option )
    {
        mCurrPage = 1;
        getLanguageAndRegionOption();
        if ( mRefreshLayout.isRefreshing() )
        {
            if ( isInternetConnected() )
            {
                performLoaderAction( option );
            }
            else
            {
                mRefreshLayout.setRefreshing( false );
                Toast.makeText(mContext,
                        getString(R.string.failed_to_refresh_no_internet_connection),
                        Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            if ( isInternetConnected() )
            {
                mMovieRecyclerViewAdapter.resetData();
                showLoading();
                performLoaderAction( option );

            }
            else
            {
                showErrorMessage();
                mEmptyTextView.setText( getString(R.string.no_internet_connection) );
            }
        }
    }

    private void performLoaderAction( LoaderOption option )
    {
        switch ( option )
        {
            case LOADER_INIT:
                getLoaderManager().initLoader(mLoaderID, null, this );
                break;
            case LOADER_RESTART:
                getLoaderManager().restartLoader(mLoaderID, null, this );
                break;
            default:
                break;
        }
    }

    private boolean isInternetConnected()
    {
        ConnectivityManager cm =
                (ConnectivityManager) getContext().getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return ( activeNetwork != null && activeNetwork.isConnected() );
    }

    private void showLoading() {

        Log.d(TAG, "showLoading: ");
        mProgressBar.setVisibility( View.VISIBLE );
        mMovieRecyclerViewAdapter.resetData();
        mFloatingActionBtn.setVisibility(View.GONE);
    }

    private void showErrorMessage() {

        Log.d(TAG, "showErrorMessage: ");
        mMovieRecyclerViewAdapter.resetData();
        mProgressBar.setVisibility( GONE );
        mRefreshLayout.setRefreshing(false);
        mEmptyTextView.setVisibility(View.VISIBLE);
        mFloatingActionBtn.setVisibility(View.GONE);
    }

    private void showData() {
        mRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(GONE);
        mEmptyTextView.setVisibility(GONE);
        mFloatingActionBtn.setVisibility(View.VISIBLE);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences( mContext );
        prefs.unregisterOnSharedPreferenceChangeListener( this );
    }

}

