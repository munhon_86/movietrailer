package com.example.movietrailerexercise.ui;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.movietrailerexercise.data.Person;
import com.example.movietrailerexercise.utilities.JsonUtil;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.io.IOException;
import java.net.URL;

public class CastDetailDataLoader extends AsyncTaskLoader<Person>
{
    private URL mSearchUrl;
    public CastDetailDataLoader(Context context, URL searchUrl)
    {
        super(context);
        mSearchUrl = searchUrl;
    }

    @Override
    public Person loadInBackground()
    {
        if (mSearchUrl == null) {
            return null;
        }

        String response = null;
        try {
            response = QueryUtil.getResponseFromHttpUrl(mSearchUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return JsonUtil.extractCastDetailData(response);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
