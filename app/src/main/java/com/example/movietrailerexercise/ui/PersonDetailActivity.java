package com.example.movietrailerexercise.ui;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movietrailerexercise.MainActivity;
import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Person;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.databinding.CastDetailViewBinding;
import com.example.movietrailerexercise.utilities.PreferenceUtil;
import com.example.movietrailerexercise.utilities.ProviderUtil;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PersonDetailActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Person>
{
    CastDetailViewBinding mBinding;
    private static final int LOADER_ID = 30;
    private static final int INVALID_CAST_ID = -1;
    private static final String INVALID_STRING = "null";
    private CastDetailCustomViewPager mViewPager;
    private TabLayout mTabLayout;
    private CastsKnownForPagerAdaptor mPagerAdapter;
    private View mInfoViewContainer;
    private int mPersonID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cast_detail_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        mPersonID = intent.getIntExtra("cast_id", INVALID_CAST_ID);

        mBinding = DataBindingUtil.setContentView(this, R.layout.cast_detail_view);
        mViewPager = findViewById(R.id.cast_detail_viewpager);
        mTabLayout = findViewById(R.id.cast_detail_tablayout);
        mInfoViewContainer = findViewById(R.id.cast_detail_container);

        SharedPreferences shrePref = PreferenceUtil.getPeopleFavoritesSharedPreference(this);

        String castIDsFavListKey = getString(R.string.people_favorites_preference_key);

        Set<String> movieIDsFavoriteList = shrePref.getStringSet(
                castIDsFavListKey,
                new HashSet<String>() );

        // check if the current movie (or tv) id is part of the favorites list
        boolean isFavorite = false;
        for (String id : movieIDsFavoriteList) {
            if ( Integer.parseInt( id ) == mPersonID)
            {
                isFavorite = true;
                break;
            }
        }
        if ( isFavorite ) {
            mBinding.castDetailContainer.favoritesCheckbox.setChecked( true );
        }

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<Person> onCreateLoader(int loaderID, Bundle bundle)
    {

        if ( mPersonID == INVALID_CAST_ID ) {
            return null;
        }

        URL searchURL = null;
        try
        {
            searchURL = QueryUtil.buildCastDetailQuery(mPersonID);
        }catch (Exception e ) {
            e.printStackTrace();
        }
        showLoading();
        return new CastDetailDataLoader( this, searchURL );
    }

    @Override
    public void onLoadFinished(Loader<Person> loader, Person person)
    {
        if ( person != null ) {
            updateView(person);
            showView();
        } else {
            showErrorMessage();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemID = item.getItemId();
        if (itemID == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if (itemID == R.id.home_icon) {
            startActivity( new Intent(this, MainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    private void updateView(final Person person)
    {
        String notAvailableMsg = getString(R.string.not_available_text_message);

        // get profile picture
        String profilePath = person.getPortraitPath();
        if (profilePath == null || profilePath.length() == 0) {
            mBinding.castDetailContainer.castProfilePicture.setImageResource(R.mipmap.ic_launcher);
        } else {
            Glide.with(this)
                    .load(profilePath)
                    .into(mBinding.castDetailContainer.castProfilePicture);
        }

        // get name
        String name = person.getName();
        mBinding.castDetailContainer.castName.setText(name);

        // get birthday
        String birthday = person.getBirthday();
        if (birthday.equals(INVALID_STRING) || birthday.length() == 0) {
            mBinding.castDetailContainer.castBirthday.setText(notAvailableMsg);
        }
        else {
            mBinding.castDetailContainer.castBirthday.setText(birthday);
        }

        // get place of birth
        String placeOfBirth = person.getBirthPlace();
        if (placeOfBirth.equals(INVALID_STRING) || placeOfBirth.length() == 0) {
            mBinding.castDetailContainer.castPlaceOfBirth.setText(notAvailableMsg);
        }
        else {
            mBinding.castDetailContainer.castPlaceOfBirth.setText(placeOfBirth);
        }

        // get profession
        String profession = person.getProfession();
        if (profession.equals(INVALID_STRING) || profession.length() == 0) {
            mBinding.castDetailContainer.castProfession.setText(notAvailableMsg);
        } else {
            mBinding.castDetailContainer.castProfession.setText(profession);
        }


        // get homepage
        String homepage = person.getHomepage();
        if (homepage.equals(INVALID_STRING)) {
            mBinding.castDetailContainer.castHomepage.setText(notAvailableMsg);
        }
        else {
            String text = "<a href='" +homepage + "'> " +homepage + "</a>";
            mBinding.castDetailContainer.castHomepage.setClickable(true);
            mBinding.castDetailContainer.castHomepage.setMovementMethod(LinkMovementMethod.getInstance());
            mBinding.castDetailContainer.castHomepage.setText(Html.fromHtml(text));
        }

        // get biography
        String biography = person.getBiography();
        if (biography.length() == 0) {
            mBinding.castDetailContainer.castBiography.setText(notAvailableMsg);
        }
        else {
            mBinding.castDetailContainer.castBiography.setText(biography);
        }


        // update both movie and tv lists the person/director known for
        List<Movie> movieList = person.getMoviesList();
        List<Movie> tvList = person.getTVsList();
        mPagerAdapter = new CastsKnownForPagerAdaptor( getSupportFragmentManager(),
                        this,
                        movieList,
                        tvList );
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(CastsKnownForPagerAdaptor.MAXIMUM_NO_OF_PAGES);
        mTabLayout.setupWithViewPager( mViewPager );

        mBinding.castDetailContainer.favoritesCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    addPersonToFavorites(person);
                } else {
                    removePersonFromFavorites(person);
                }
            }
        });
    }

    private void addPersonToFavorites(Person person) {
        Uri newUri = ProviderUtil.savePersonIntoDatabase(this, person);
        if ( newUri != null ) {
            Toast.makeText(this,
                    getString(R.string.movie_added_to_favorites, person.getName()),
                    Toast.LENGTH_SHORT)
                    .show();
        }
        else
        {
            Toast.makeText(this,
                    "Failed to save into database",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void removePersonFromFavorites(Person person) {
        int personID = person.getID();
        int rowsDeleted = ProviderUtil.deletePerson(this, personID);
        String toastMessage;
        if (rowsDeleted > 0) {
            toastMessage = getString(R.string.movie_deleted_from_favorites_success,
                    person.getName());
        }
        else {
            toastMessage = getString(R.string.movie_deleted_from_favorites_fail);
        }
        Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoaderReset(Loader<Person> loader) {

    }

    private void showLoading() {
        mInfoViewContainer.setVisibility(View.GONE);
        mBinding.emptyDataTextview.setVisibility(View.GONE);
        mBinding.loadingSpinner.setVisibility(View.VISIBLE);
        mBinding.castDetailContainer.castProfilePicture.setVisibility(View.GONE);
        mBinding.castDetailContainer.favoritesCheckbox.setVisibility(View.GONE);
    }

    private void showView() {
        mInfoViewContainer.setVisibility(View.VISIBLE);
        mBinding.emptyDataTextview.setVisibility(View.GONE);
        mBinding.loadingSpinner.setVisibility(View.GONE);
        mBinding.castDetailContainer.castProfilePicture.setVisibility(View.VISIBLE);
        mBinding.castDetailContainer.favoritesCheckbox.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage() {
        mBinding.emptyDataTextview.setText(getString(R.string.failed_to_load_detail_movie_data));
        mInfoViewContainer.setVisibility(View.GONE);
        mBinding.emptyDataTextview.setVisibility(View.VISIBLE);
        mBinding.loadingSpinner.setVisibility(View.GONE);
        mBinding.castDetailContainer.castProfilePicture.setVisibility(View.GONE);
        mBinding.castDetailContainer.favoritesCheckbox.setVisibility(View.GONE);
    }
}
