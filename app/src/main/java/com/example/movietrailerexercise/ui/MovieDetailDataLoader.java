package com.example.movietrailerexercise.ui;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.utilities.JsonUtil;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.io.IOException;
import java.net.URL;

public class MovieDetailDataLoader extends AsyncTaskLoader<Movie>
{
    private static final String LOG_TAG = QueryDataLoader.class.getSimpleName();
    private URL mSearchUrl;
    private Context mContext;
    private boolean mIsTVSelected;

    public MovieDetailDataLoader(Context context, URL queryURL, boolean isTVSelected)
    {
        super(context);
        mContext = context;
        mSearchUrl = queryURL;
        mIsTVSelected = isTVSelected;
        Log.v(LOG_TAG, "Loader created ");
    }

    @Override
    public Movie loadInBackground() {
        Log.v(LOG_TAG, "Loading in background ");
        if ( mSearchUrl == null )
        {
            return null;
        }

        String searchResponse;
        try {
            searchResponse = QueryUtil.getResponseFromHttpUrl(mSearchUrl);
        } catch (IOException e)
        {
            return null;
        }

        return JsonUtil.extractDetailMovieDataList( searchResponse, mIsTVSelected );

    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }



}
