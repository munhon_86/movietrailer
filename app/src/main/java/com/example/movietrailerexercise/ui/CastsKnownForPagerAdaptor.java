package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;

import java.util.List;

public class CastsKnownForPagerAdaptor extends FragmentPagerAdapter {
    private Context mContext;
    private int mCurrPosition = -1;

    private static final int MOVIES = 0;
    private static final int TVS = 1;
    public static final int MAXIMUM_NO_OF_PAGES = 2;
    private List<Movie> mMovieList;
    private List<Movie> mTVList;
    public CastsKnownForPagerAdaptor(FragmentManager fm,
                                     Context context,
                                     List<Movie> movieList,
                                     List<Movie> tvList) {
        super(fm);
        mContext = context;
        mMovieList = movieList;
        mTVList = tvList;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == MOVIES) {
            return MoviesListHorizontalViewFragment.newInstance(mMovieList, false);
        }
        else
        {
            return MoviesListHorizontalViewFragment.newInstance(mTVList, true);
        }
    }

    @Override
    public int getCount() {
        return MAXIMUM_NO_OF_PAGES;
    }

    public CharSequence getPageTitle(int position) {
        String pageTitle = null;
        switch ( position )
        {
            case MOVIES:
                pageTitle = mContext.getString(R.string.favorites_movies_tab_string);
                break;
            case TVS:
                pageTitle = mContext.getString(R.string.favorites_tvs_tab_string);
                break;
            default:
                break;
        }
        return pageTitle;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (mCurrPosition != position) {
            Fragment fragment = (Fragment) object;
            CastDetailCustomViewPager customViewPager = (CastDetailCustomViewPager) container;
            if (fragment != null && fragment.getView() != null) {
                mCurrPosition = position;
                customViewPager.measureCurrentView(fragment.getView());
            }
        }
    }
}
