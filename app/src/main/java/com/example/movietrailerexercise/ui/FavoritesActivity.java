package com.example.movietrailerexercise.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.utilities.PreferenceUtil;
import com.example.movietrailerexercise.utilities.ProviderUtil;

import java.util.HashSet;
import java.util.Set;

public class FavoritesActivity extends AppCompatActivity implements
        DeleteFavoritesAlertDialog.DeleteMovieListener, FavoritesListFragment.OnLoaderLoadFinished
{
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private static boolean sIsDatabaseEmpty;
    private static int sNumOfMoviesFavorites;
    private static int sNumOfTVsFavorites;
    private static int sNumOfPeopleFavorites;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorites_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mTabLayout = findViewById(R.id.favorites_activity_tablayout);
        mViewPager = findViewById(R.id.favorites_activity_viewpager);

        PagerAdapter pagerAdapter =
                new FavoritesPagerAdaptor( getSupportFragmentManager(),this );
        mViewPager.setAdapter( pagerAdapter );
        mViewPager.setOffscreenPageLimit(FavoritesPagerAdaptor.MAXIMUM_NO_OF_PAGES);
        mTabLayout.setupWithViewPager( mViewPager );

        updateNumberOfFavoritesCount();
        updateIsDatabaseEmpty();
    }

    private void updateNumberOfFavoritesCount() {
        for ( int i=0; i<FavoritesPagerAdaptor.MAXIMUM_NO_OF_PAGES; i++ ) {
            String favoritesListKey = null;
            SharedPreferences sharePref = null;
            switch (i) {
                case FavoritesPagerAdaptor.MOVIES:
                    favoritesListKey = PreferenceUtil.getMovieFavoritesListKey(false, this);
                    sharePref = PreferenceUtil.getMovieFavoritesSharedPreference(false, this);
                    break;
                case FavoritesPagerAdaptor.TVS:
                    favoritesListKey = PreferenceUtil.getMovieFavoritesListKey(true, this);
                    sharePref = PreferenceUtil.getMovieFavoritesSharedPreference(true, this);
                    break;
                case FavoritesPagerAdaptor.PEOPLE:
                    favoritesListKey = getString(R.string.people_favorites_preference_key);
                    sharePref = PreferenceUtil.getPeopleFavoritesSharedPreference(this);
                    break;
                default:
                    break;
            }

            Set<String> favoritesList = sharePref.getStringSet(
                    favoritesListKey,
                    new HashSet<String>());

            switch (i) {
                case FavoritesPagerAdaptor.MOVIES:
                    sNumOfMoviesFavorites = favoritesList.size();
                    break;
                case FavoritesPagerAdaptor.TVS:
                    sNumOfTVsFavorites = favoritesList.size();
                    break;
                case FavoritesPagerAdaptor.PEOPLE:
                    sNumOfPeopleFavorites = favoritesList.size();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorites_menu, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if ( id == R.id.delete_all_movies )
        {
            DeleteFavoritesAlertDialog alertDialog = new DeleteFavoritesAlertDialog();
            alertDialog.setDeleteAllMovies( true );
            alertDialog.setCallbackListener( this );
            alertDialog.show( getSupportFragmentManager(), "delete_all_alert_dialog");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfirmDelete() {
        int rowsDeleted = ProviderUtil.deleteAllFavorites( this );
        if ( rowsDeleted > 0 )
        {
            Toast.makeText(this,
                    rowsDeleted +" favorites deleted ",
                    Toast.LENGTH_SHORT ).show();
            sIsDatabaseEmpty = true;
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if ( sIsDatabaseEmpty ) {
            menu.findItem(R.id.delete_all_movies).setEnabled(false);
        }
        else
        {
            menu.findItem(R.id.delete_all_movies).setEnabled(true);
        }
        return true;
        //return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCursorLoadFinished(int cursorCount, int loaderID ) {

        switch (loaderID) {
            case FavoritesListFragment.FAVORITES_MOVIE_LOADER_ID:
                sNumOfMoviesFavorites = cursorCount;
                break;
            case FavoritesListFragment.FAVORITES_TV_LOADER_ID:
                sNumOfTVsFavorites = cursorCount;
                break;
            case FavoritesListFragment.FAVORITES_PEOPLE_LOADER_ID:
                sNumOfPeopleFavorites = cursorCount;
                break;
            default:
                break;
        }

        updateIsDatabaseEmpty();
    }

    private void updateIsDatabaseEmpty()
    {
        if ((sNumOfMoviesFavorites == 0) && (sNumOfTVsFavorites == 0)
                && (sNumOfPeopleFavorites == 0)) {
            sIsDatabaseEmpty = true;
        }
        else {
            sIsDatabaseEmpty = false;
        }
    }
}
