package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.data.MovieContract;
import com.example.movietrailerexercise.utilities.ProviderUtil;
import com.example.movietrailerexercise.utilities.ViewUtil;
//import com.nostra13.universalimageloader.core.ImageLoader;

public class FavoritesMovieRecyclerViewAdapter extends
        RecyclerView.Adapter<FavoritesMovieRecyclerViewAdapter.FavoritesAdapterViewHolder> implements
        DeleteFavoritesAlertDialog.DeleteMovieListener
{

    /*
     * Below, we've defined an interface to handle clicks on items within this Adapter. In the
     * constructor of our MovieAdapterOnClickHandler, we receive an instance of a class that has implemented
     * said interface. We store that instance in this variable to call the onClick method whenever
     * an item is clicked in the list.
     */

    private Context mContext;
    private Cursor  mCursor;
    private android.support.v7.view.ActionMode mActionMode;
    private boolean mIsTVTab;

    private android.support.v7.view.ActionMode.Callback mActionModeCallback
            = new android.support.v7.view.ActionMode.Callback()
    {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_menu_delete:
                    //deleteMovie();
                    DeleteFavoritesAlertDialog alertDialog = new DeleteFavoritesAlertDialog();
                    alertDialog.setCallbackListener( FavoritesMovieRecyclerViewAdapter.this);
                    alertDialog.show( ((AppCompatActivity) mContext).getSupportFragmentManager(),
                                       "deleteMoviesAlert");
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {
            mActionMode = null;
        }
    };

    /**
     * The interface that receives onClick messages.
     */

    /**
     * Creates a MovieRecyclerViewAdapter.
     *
     * @param context      Used to talk to the UI and app resources
     */
    public FavoritesMovieRecyclerViewAdapter(@NonNull Context context, boolean isTVTab) {
        mContext = context;
        mIsTVTab = isTVTab;
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param parent    The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (like ours does) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new MovieAdapterViewHolder that holds the View for each list item
     */
    @Override
    public FavoritesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(mContext).inflate(R.layout.movietabview, parent, false);
        View view = LayoutInflater.from(mContext).inflate(R.layout.movie_image_tab, parent, false);
        view.setFocusable(true);

        return new FavoritesAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavoritesAdapterViewHolder holder, int position)
    {
        mCursor.moveToPosition(position);
        int posterColumnIndex =
                mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_MOVIE_POSTER);
        String posterPath = mCursor.getString( posterColumnIndex );
        Glide.with(mContext).load(posterPath).into(holder.mMovieImage);

        int titleColumnIndex =
                mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_MOVIE_TITLE);
        String title = mCursor.getString( titleColumnIndex );
        holder.mMovieTitle.setText( title );

        int voteAvgColumnIndex =
                mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_MOVIE_AVERAGE_VOTE);
        double voteAvg = mCursor.getDouble( voteAvgColumnIndex );
        ViewUtil.updateTextView( holder.mVoteAverage, voteAvg, mContext);
    }


    @Override
    public int getItemCount() {
        if ( mCursor == null )
        {
            return 0;
        }
        return mCursor.getCount();
    }

    public class FavoritesAdapterViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener, View.OnLongClickListener {
        final ImageView mMovieImage;
        final TextView mMovieTitle;
        final TextView mVoteAverage;

        FavoritesAdapterViewHolder(View view) {
            super(view);
            mMovieImage = view.findViewById(R.id.thumbnail);
            mMovieTitle = view.findViewById(R.id.movie_title);
            mVoteAverage = view.findViewById(R.id.movie_vote_average_textview);
            view.setOnClickListener(this);
            view.setOnLongClickListener( this );
        }

        /**
         * This gets called by the child views during a click. We fetch the date that has been
         * selected, and then call the onClick handler registered with this adapter, passing that
         * position.
         *
         * @param view the View that was clicked
         */
        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            mCursor.moveToPosition(adapterPosition);
            int movieIDColumnIndex =
                    mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_MOVIE_ID);
            int movieID = mCursor.getInt( movieIDColumnIndex );
            Intent startMovieDetailIntent = new Intent(mContext, MovieDetailActivity.class);
            startMovieDetailIntent.putExtra(Movie.MOVIE_ID_BUNDLE_KEY, movieID);
            startMovieDetailIntent.putExtra(Movie.MOVIE_IS_TV_SELECTED_BUNDLE_KEY, mIsTVTab);
            mContext.startActivity(startMovieDetailIntent);
        }

        @Override
        public boolean onLongClick(View view)
        {
            if (mActionMode != null) {
                return false;
            }

            int adapterPosition = getAdapterPosition();
            mCursor.moveToPosition( adapterPosition );

            // Start the CAB using the ActionMode.Callback defined above
            mActionMode =  ((AppCompatActivity)mContext)
                    .startSupportActionMode(mActionModeCallback);
            view.setSelected(true);
            return true;
        }
    }

    public void swapCursor( Cursor cursor )
    {
        mCursor = cursor;
        notifyDataSetChanged();
    }

    private void deleteMovie()
    {
        int movieIDColumnIndex = mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_MOVIE_ID);
        int movieID = mCursor.getInt( movieIDColumnIndex );
        ProviderUtil.deleteMovie( mContext, movieID, mIsTVTab );
    }

    @Override
    public void onConfirmDelete() {
        deleteMovie();
    }
}

