package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;

import java.util.ArrayList;
import java.util.List;

public class MoviesListHorizontalViewFragment extends Fragment
{
    private static final String MOVIE_LIST_KEY = "movie_list";
    private RecyclerView mMovieListsRecyclerView;
    private TextView     mErrorText;
    private boolean      mIsTVSelected;
    private MovieRecyclerViewAdapter mAdapter;

    public static MoviesListHorizontalViewFragment newInstance(List<Movie> movieList,
                                                               boolean isTVSelected)
    {
        MoviesListHorizontalViewFragment fragment = new MoviesListHorizontalViewFragment();
        if ( movieList != null )
        {
            Bundle arg = new Bundle();
            ArrayList<Movie> movieArrayList = new ArrayList<>(movieList);
            //arg.putSerializable(MOVIE_LIST_KEY, movieList.toArray( new Movie[movieList.size()]));
            arg.putParcelableArrayList(MOVIE_LIST_KEY,movieArrayList);
            arg.putBoolean(Movie.MOVIE_IS_TV_SELECTED_BUNDLE_KEY, isTVSelected);
            fragment.setArguments(arg);
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.movies_list_view_horizontal,
                container,
                false);
        mMovieListsRecyclerView = view.findViewById(R.id.movies_list_recycler_view);
        mErrorText = view.findViewById(R.id.movies_list_not_available_text_view);

        Context context = getContext();

        LinearLayoutManager movieListLayoutManager =
/*                new LinearLayoutManager(
                        context,
                        LinearLayoutManager.HORIZONTAL,
                        false);*/
        new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        Bundle args = getArguments();
        if (args != null && args.containsKey(Movie.MOVIE_IS_TV_SELECTED_BUNDLE_KEY)) {
            mIsTVSelected = args.getBoolean(Movie.MOVIE_IS_TV_SELECTED_BUNDLE_KEY);
        }

        mAdapter = new MovieRecyclerViewAdapter(context, mIsTVSelected, true);

        if ( args != null && args.containsKey(MOVIE_LIST_KEY) )
        {
            ArrayList<Movie> movieList =
                    args.getParcelableArrayList(MOVIE_LIST_KEY);
            //List<Movie> movieList1 = Arrays.asList(movieList);
            if ( (movieList != null) && (movieList.size() > 0) ) {
                mMovieListsRecyclerView.setLayoutManager(movieListLayoutManager);
                mMovieListsRecyclerView.setAdapter(mAdapter);
                mAdapter.updateDataList( movieList );
                mMovieListsRecyclerView.setHasFixedSize( true );
                mMovieListsRecyclerView.setVisibility(View.VISIBLE);
                mErrorText.setVisibility(View.GONE);
            }
            else {
                showErrorMessage();
            }

        }
        else
        {
            showErrorMessage();
        }

        return view;
    }

    public void showErrorMessage() {
        mMovieListsRecyclerView.setVisibility(View.GONE);
        mErrorText.setText( getString(R.string.not_available_text_message) );
        mErrorText.setVisibility(View.VISIBLE);
    }

}
