package com.example.movietrailerexercise.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Person;

import java.util.List;

public class CastsListRecyclerViewAdapter extends
        RecyclerView.Adapter<CastsListRecyclerViewAdapter.CastViewHolder>
{
    private Context mContext;
    private List<Person> mCastsList;
    public CastsListRecyclerViewAdapter(@NonNull Context context, List<Person> personList) {
        mContext = context;
        mCastsList = personList;
    }

    @NonNull
    @Override
    public CastsListRecyclerViewAdapter.CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( mContext ).inflate(
                R.layout.casts_listview,
                parent,
                false);
        return new CastViewHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull CastsListRecyclerViewAdapter.CastViewHolder holder,
                                 int position)
    {
        Person person = mCastsList.get( position );

        String castPortraitPath = person.getPortraitPath();
        if (castPortraitPath == null ) {
            holder.mCastPortrait.setImageResource(R.mipmap.ic_launcher);
        }
        else {
            Glide.with(mContext).load(castPortraitPath).into(holder.mCastPortrait);
        }

        String castName = person.getName();
        holder.mCastName.setText( castName );

    }

    @Override
    public int getItemCount() {
        return mCastsList.size();
    }


    public class CastViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public ImageView mCastPortrait;
        public TextView  mCastName;
        public CastViewHolder(View view )
        {
            super(view);
            mCastPortrait = view.findViewById(R.id.cast_portrait);
            mCastName = view.findViewById(R.id.cast_name);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ( (Activity) mContext ).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // This code is used to get the screen dimensions of the user's device
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;

            // Set the ViewHolder width to be a third of the screen size, and height to wrap content
            itemView.setLayoutParams(new RecyclerView.LayoutParams(
                    (int) Math.round(width * 0.6),
                    RecyclerView.LayoutParams.MATCH_PARENT
            ));
            view.setOnClickListener( this );
        }

        @Override
        public void onClick(View view)
        {
            int adapterPosition = getAdapterPosition();
            int castID = mCastsList.get(adapterPosition).getID();
            Intent startCastActivity = new Intent(mContext, PersonDetailActivity.class);
            startCastActivity.putExtra("cast_id", castID);
            mContext.startActivity(startCastActivity);
        }
    }
}
