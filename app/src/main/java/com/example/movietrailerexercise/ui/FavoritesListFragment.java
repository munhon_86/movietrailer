package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.MovieContract;


public class FavoritesListFragment extends Fragment implements
        android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>
{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mMovieAdapter;
    private ProgressBar mProgressBar;
    private TextView mEmptyTextView;
    private SwipeRefreshLayout mRefreshLayout;
    private FloatingActionButton mTopFloatingActionBtn;

    private Context mContext;

    private static final String TAG = "FavoritesListFragment";

    private int mLoaderID;

    private static final String ARGUMENTS_POSITION_KEY = "position";
    private static final int FAVORITES_LOADER_OFFSET = 10;
    public static final int FAVORITES_MOVIE_LOADER_ID = 10;
    public static final int FAVORITES_TV_LOADER_ID = 11;
    public static final int FAVORITES_PEOPLE_LOADER_ID = 12;
    //public static final int FAVORITES_DIRECTOR_LOADER_ID = 13;

    private OnLoaderLoadFinished mListener;

    private boolean mIsMovieOrTV;
    //private boolean mCastTab;

    public interface OnLoaderLoadFinished
    {
        void onCursorLoadFinished(int cursorCount, int loaderID );
    }


    public static FavoritesListFragment newInstance (int position) {
        Bundle args = new Bundle();
        FavoritesListFragment favoritesListFragment = new FavoritesListFragment();
        args.putInt(ARGUMENTS_POSITION_KEY, position);
        favoritesListFragment.setArguments(args);
        return favoritesListFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        mRecyclerView = view.findViewById(R.id.fragment_movielist_recycler_view);
        mProgressBar = view.findViewById(R.id.loading_spinner);
        mEmptyTextView = view.findViewById(R.id.empty_data_textview);
        mRefreshLayout = view.findViewById(R.id.swiperefresh);
        mTopFloatingActionBtn = view.findViewById(R.id.top_floating_action_btn);
        mTopFloatingActionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        mContext = getContext();

        mRefreshLayout.setEnabled( false );

        if ( getArguments().containsKey(ARGUMENTS_POSITION_KEY) )
        {
            mLoaderID = getArguments().getInt(ARGUMENTS_POSITION_KEY);
            mLoaderID += FAVORITES_LOADER_OFFSET;
            switch (mLoaderID) {
                case FAVORITES_TV_LOADER_ID:
                    mIsMovieOrTV = true;
                    break;
                case FAVORITES_PEOPLE_LOADER_ID:
                    mIsMovieOrTV = false;
                    break;
                default:
                    break;
            }

        }

        Log.v(TAG, "onCreateView: ");

        GridLayoutManager layoutManager =
                new GridLayoutManager(mContext, 2 );
/*        LinearLayoutManager layoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);*/

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        if (mLoaderID == FAVORITES_MOVIE_LOADER_ID || mLoaderID == FAVORITES_TV_LOADER_ID) {
            mMovieAdapter = new FavoritesMovieRecyclerViewAdapter(mContext, mIsMovieOrTV);
        } else {
            mMovieAdapter = new FavoritesPeopleRecyclerViewAdapter(mContext);
        }

        /* Setting the adapter attaches it to the RecyclerView in our layout. */
        mRecyclerView.setAdapter(mMovieAdapter);

        getLoaderManager().initLoader(mLoaderID, null, this);


        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        String[] projection = getDatabaseProjection();

        Uri contentUri = getDatabaseContentUri();

        Log.v(TAG, "onCreateLoader: " +contentUri.toString());
        // Query the database; sort the list by latest added favorites
        //String sortOrder = MovieContract.MovieEntry._ID + " ASC ";
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return new android.support.v4.content.CursorLoader(getContext(),
                contentUri,
                projection,
                null,
                null,
                null );
    }

    private Uri getDatabaseContentUri() {
        Uri contentUri;
        if (mLoaderID == FAVORITES_MOVIE_LOADER_ID || mLoaderID == FAVORITES_TV_LOADER_ID) {
            if (mIsMovieOrTV) {
                contentUri = MovieContract.MovieEntry.TV_CONTENT_URI;
            } else {
                contentUri = MovieContract.MovieEntry.MOVIE_CONTENT_URI;
            }
        } else {
            contentUri = MovieContract.MovieEntry.PEOPLE_CONTENT_URI;
        }
        return contentUri;
    }

    private String[] getDatabaseProjection() {
        if (mLoaderID == FAVORITES_MOVIE_LOADER_ID || mLoaderID == FAVORITES_TV_LOADER_ID) {
            return new String[] {MovieContract.MovieEntry._ID,
                    MovieContract.MovieEntry.COLUMN_MOVIE_ID,
                    MovieContract.MovieEntry.COLUMN_MOVIE_POSTER,
                    MovieContract.MovieEntry.COLUMN_MOVIE_TITLE,
                    MovieContract.MovieEntry.COLUMN_MOVIE_AVERAGE_VOTE};

        }
        else {
            return new String[] {MovieContract.MovieEntry._ID,
                    MovieContract.MovieEntry.COLUMN_PERSON_ID,
                    MovieContract.MovieEntry.COLUMN_PERSON_POSTER_PATH,
                    MovieContract.MovieEntry.COLUMN_PERSON_NAME};
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        data.moveToNext();
        if (mLoaderID == FAVORITES_MOVIE_LOADER_ID || (mLoaderID ==FAVORITES_TV_LOADER_ID)){
            ((FavoritesMovieRecyclerViewAdapter)mMovieAdapter).swapCursor(data);
        } else {
            ((FavoritesPeopleRecyclerViewAdapter)mMovieAdapter).swapCursor(data);
        }
        int numOfCounts = data.getCount();
        if ( numOfCounts == 0 )
        {
            mEmptyTextView.setText( getString(R.string.no_favorites_added) );
            showErrorMessage();
        }
        else
        {
            showData();
        }

        mListener.onCursorLoadFinished(numOfCounts, mLoaderID);
    }

    private void showData() {
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mEmptyTextView.setVisibility(View.GONE);
        mTopFloatingActionBtn.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage() {
        mRecyclerView.setVisibility( View.INVISIBLE );
        mProgressBar.setVisibility( View.GONE );
        mEmptyTextView.setVisibility( View.VISIBLE );
        mTopFloatingActionBtn.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (OnLoaderLoadFinished) getActivity();
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(getActivity().getClass().toString()
                    + " must implement NoticeDialogListener");
        }
    }
}
