package com.example.movietrailerexercise.ui;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Person;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.data.QueryResponse;
import com.example.movietrailerexercise.utilities.QueryUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class SearchableActivity extends AppCompatActivity implements
        android.support.v4.app.LoaderManager.LoaderCallbacks<QueryResponse> {

    private static final int SEARCH_LOADER_ID = 20;
    private static final String LOADER_USER_QUERY_ARGS = "query_string";
    private int mCurrPage;

    // views variable
    private View mMainContainer;
    private ProgressBar mProgressBar;
    private TextView mMovieLabel;
    private TextView mTVLabel;
    private TextView mPeopleLabel;
    private RecyclerView mPeopleRecyclerView;
    private RecyclerView mMovieRecyclerView;
    private RecyclerView mTVRecyclerView;
    private TextView     mErrorText;

    private static final String TAG = "SearchableActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_searchable);

        mMainContainer = findViewById(R.id.acitivity_search_results_container);
        mPeopleLabel = findViewById(R.id.people_label_tv);
        mMovieLabel = findViewById(R.id.movie_label_tv);
        mTVLabel = findViewById(R.id.tv_label_tv);
        mPeopleRecyclerView = findViewById(R.id.people_search_results_recyclerview);
        mMovieRecyclerView = findViewById(R.id.movie_search_results_recyclerview);
        mTVRecyclerView = findViewById(R.id.tv_search_results_recyclerview);
        mProgressBar = findViewById(R.id.loading_spinner);
        mErrorText = findViewById(R.id.error_text_tv);

        mCurrPage = 1;

        handleIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null && query.length() > 0) {
                Bundle args = new Bundle();
                args.putString(LOADER_USER_QUERY_ARGS, query);
                getSupportLoaderManager().restartLoader(SEARCH_LOADER_ID, args, this);
            }
        }
    }

    @NonNull
    @Override
    public Loader<QueryResponse> onCreateLoader(int loaderID, @Nullable Bundle args) {
        String query = null;
        showLoading();
        if (args != null && args.containsKey(LOADER_USER_QUERY_ARGS)) {
            query = args.getString(LOADER_USER_QUERY_ARGS, null);
        }

        URL url = null;
        if (query != null && query.length() > 0) {
            try {
                url = QueryUtil.buildUserSearchQuery(query, mCurrPage);
                Log.d(TAG, "onCreateLoader: url = " +url);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new QueryDataLoader(this, url,null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<QueryResponse> loader, QueryResponse data) {
        if (data != null) {
            updateView(data);
        } else {
            showErrorMessage();
        }
    }

    private void showLoading() {
        mMainContainer.setVisibility(View.GONE);
        mErrorText.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage() {
        mMainContainer.setVisibility(View.GONE);
        mErrorText.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    private void updateView(QueryResponse data) {
        List<Movie> movieList = data.getMovieList();
        List<Movie> tvList = data.getTVList();
        List<Person>  peopleList = data.getCastList();

        showData();

        if (movieList != null && movieList.size() > 0) {
            showMovieResultsView(movieList);
        } else {
            hideMovieResultsView();
        }

        if (tvList != null && tvList.size() > 0) {
            showTVResultsView(tvList);
        } else {
            hideTVResultsView();
        }

        if (peopleList != null && peopleList.size() > 0) {
            showPeopleResultsView(peopleList);
        } else {
            hidePeopleResultsView();
        }

    }

    private void hidePeopleResultsView() {
        mPeopleLabel.setVisibility(View.GONE);
        mPeopleRecyclerView.setVisibility(View.GONE);
    }

    private void showPeopleResultsView(List<Person> peopleList) {
        CastsListRecyclerViewAdapter adapter = new CastsListRecyclerViewAdapter(
                this,
                peopleList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.HORIZONTAL,
                false);
        mPeopleRecyclerView.setAdapter(adapter);
        mPeopleRecyclerView.setLayoutManager(layoutManager);
        mPeopleLabel.setVisibility(View.VISIBLE);
        mPeopleRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hideTVResultsView() {
        mTVLabel.setVisibility(View.GONE);
        mTVRecyclerView.setVisibility(View.GONE);
    }

    private void showTVResultsView(List<Movie> tvList) {
        MovieRecyclerViewAdapter adapter = new MovieRecyclerViewAdapter(
                this,
                true,
                true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.HORIZONTAL,
                false);
        mTVRecyclerView.setAdapter(adapter);
        mTVRecyclerView.setLayoutManager(layoutManager);
        adapter.updateDataList(tvList);
        mTVLabel.setVisibility(View.VISIBLE);
        mTVRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hideMovieResultsView() {
        mMovieLabel.setVisibility(View.GONE);
        mMovieRecyclerView.setVisibility(View.GONE);
    }

    private void showMovieResultsView(List<Movie> movieList) {
        MovieRecyclerViewAdapter adapter = new MovieRecyclerViewAdapter(
                this,
                false,
                true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.HORIZONTAL,
                false);
        mMovieRecyclerView.setAdapter(adapter);
        mMovieRecyclerView.setLayoutManager(layoutManager);
        adapter.updateDataList(movieList);
        mMovieLabel.setVisibility(View.VISIBLE);
        mMovieRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showData() {
        mMainContainer.setVisibility(View.VISIBLE);
        mErrorText.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<QueryResponse> loader) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemID = item.getItemId();
        if (itemID == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
