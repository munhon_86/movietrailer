package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.movietrailerexercise.R;

public class FavoritesPagerAdaptor extends FragmentPagerAdapter {

    private Context mContext;

    public static final int MOVIES = 0;
    public static final int TVS = 1;
    public static final int PEOPLE = 2;
    //public static final int DIRECTOR = 3;
    public static final int MAXIMUM_NO_OF_PAGES = 3;

    public FavoritesPagerAdaptor(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return FavoritesListFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return MAXIMUM_NO_OF_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String pageTitle = null;
        switch (position) {
            case MOVIES:
                pageTitle = mContext.getString(R.string.favorites_movies_tab_string);
                break;
            case TVS:
                pageTitle = mContext.getString(R.string.favorites_tvs_tab_string);
                break;
            case PEOPLE:
                pageTitle = mContext.getString(R.string.favorites_people_tab_string);
                break;
            default:
                break;
        }
        return pageTitle;
    }

}

