package com.example.movietrailerexercise.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.example.movietrailerexercise.R;

public class SettingsFragment extends PreferenceFragmentCompat implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String prefKey)
    {
        Preference pref = findPreference( prefKey );
        if ( pref instanceof android.support.v7.preference.ListPreference )
        {
            String value = sharedPreferences.getString(prefKey, "");
            setPreferenceSummary( pref, value );
        }
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.pref_general);
        android.support.v7.preference.PreferenceScreen prefScreen = getPreferenceScreen();
        int numOfPreferences = prefScreen.getPreferenceCount();
        SharedPreferences sharedPreferences = prefScreen.getSharedPreferences();
        for ( int i=0; i<numOfPreferences; i++ )
        {
            Preference pref = prefScreen.getPreference( i );
            if ( !(pref instanceof CheckBoxPreference) )
            {
                String prefKey = pref.getKey();
                String value = sharedPreferences.getString(prefKey, "");
                setPreferenceSummary( pref, value );
            }
        }
    }

    private void setPreferenceSummary( Preference pref, String value )
    {

        if ( pref instanceof android.support.v7.preference.ListPreference )
        {
            android.support.v7.preference.ListPreference listPreference =
                    (android.support.v7.preference.ListPreference) pref;
            int prefIndex = listPreference.findIndexOfValue(value);
            if (prefIndex >= 0) {
                pref.setSummary(listPreference.getEntries()[prefIndex]);
            }
        }
        else
        {
            pref.setSummary( value );
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener( this );
    }

    @Override
    public void onStop() {
        super.onStop();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener( this );
    }
}
