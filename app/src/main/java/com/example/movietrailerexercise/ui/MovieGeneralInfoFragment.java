package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.Movie;
import com.example.movietrailerexercise.databinding.ActivityDetailBinding;
import com.example.movietrailerexercise.utilities.ViewUtil;

import java.util.List;

public class MovieGeneralInfoFragment extends Fragment {
    private static final String IS_TV_FLAG_KEY = "tv_flag";
    private static final String MOVIE_DATA = "movie";
    private static final String NULL_STRING = "null";

    private ActivityDetailBinding mBinding;

    public static MovieGeneralInfoFragment newInstance(boolean isTVSelected, Movie movie) {
        MovieGeneralInfoFragment fragment = new MovieGeneralInfoFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_TV_FLAG_KEY, isTVSelected);
        args.putParcelable(MOVIE_DATA, movie);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        boolean isTVSelected = getArguments().getBoolean(IS_TV_FLAG_KEY, false);
        Movie movie = getArguments().getParcelable(MOVIE_DATA);

        int res = isTVSelected ?
                R.layout.detail_tv_general_info_card : R.layout.detail_movie_general_info_card;
        View view = inflater.inflate(res, container, false);

        if (movie != null) {
            updateCommonViewInfo(view, movie);

            if (isTVSelected) {
                updateTVGeneralInfoView(view, movie);
            } else {
                updateMovieGeneralInfoView(view, movie);
            }
        }

        return view;
    }

    private void updateCommonViewInfo(View view, Movie movie) {
        Context context = getContext();
        String notAvailableMsg = getString(R.string.not_available_text_message);

        TextView genres = view.findViewById(R.id.genres_textview);
        TextView status = view.findViewById(R.id.status_textview);
        TextView avgVote = view.findViewById(R.id.movie_vote_average_textview);
        TextView voteCount = view.findViewById(R.id.votes_count_textview);
        TextView homepageTextView = view.findViewById(R.id.homepage_textview);
        TextView runtimeTextView = view.findViewById(R.id.runtime_textview);

        // set movie genres
        if (movie.getGenresList() != null && movie.getGenresList().size() > 0) {
            String genresText = TextUtils.join(", ", movie.getGenresList());
            genres.setText(genresText);
        }else {
            genres.setText(notAvailableMsg);
        }

        // set status
        String statusText = movie.getStatus();
        if (statusText != null && !statusText.equals(NULL_STRING) && statusText.length() > 0) {
            status.setText(statusText);
        } else {
            status.setText(notAvailableMsg);
        }

/*        // set movie vote average
        ViewUtil.updateTextView(avgVote, movie.getMovieAverageVote(), context);

        // set movie number of votes
        int numOfVotes = movie.getVoteCount();
        String numOfVotesFormatted = getString(R.string.number_of_votes_format_string, numOfVotes);
        voteCount.setText(numOfVotesFormatted);*/

        // set homepage
        String homepage = movie.getHomepage();
        if (!homepage.equals(NULL_STRING) && homepage.length() > 0) {
            String text = "<a href='" +homepage + "'> " +homepage + "</a>";
            homepageTextView.setClickable(true);
            homepageTextView.setMovementMethod(LinkMovementMethod.getInstance());
            homepageTextView.setText(Html.fromHtml(text));
        } else {
            homepageTextView.setText(notAvailableMsg);
        }

        // set runtime
        int runtime = movie.getRuntime();
        if (runtime > 0) {
            String runtimeText = getString(R.string.runtime_message, runtime);
            runtimeTextView.setText(runtimeText);
        } else {
            runtimeTextView.setText(notAvailableMsg);
        }

    }

    private void updateTVGeneralInfoView(View view, Movie movie) {
        String notAvailableMsg = getString(R.string.not_available_text_message);

        TextView firstAirDateTextView = view.findViewById(R.id.first_air_date_textview);
        TextView lastAirDateTextView = view.findViewById(R.id.last_air_date_textview);
        TextView numOfEpisodesTextView = view.findViewById(R.id.number_of_episodes_textview);
        TextView numOfSeasonsTextView = view.findViewById(R.id.number_of_seasons_textview);

        // set first air date
        String firstAirDate = movie.getFirstAirDate();
        if (firstAirDate != null && !firstAirDate.equals(NULL_STRING) && firstAirDate.length() > 0) {
            firstAirDateTextView.setText(firstAirDate);
        } else {
            firstAirDateTextView.setText(notAvailableMsg);
        }

        // set last air date
        String lastAirDate = movie.getLastAirDate();
        if (lastAirDate != null && !lastAirDate.equals(NULL_STRING) && lastAirDate.length() > 0) {
            lastAirDateTextView.setText(lastAirDate);
        } else {
            lastAirDateTextView.setText(notAvailableMsg);
        }

        // set number of episodes
        int numOfEpisodes = movie.getNumberOfEpisodes();
        if (numOfEpisodes > 0) {
            numOfEpisodesTextView.setText(String.valueOf(numOfEpisodes));
        } else {
            numOfEpisodesTextView.setText(notAvailableMsg);
        }

        // set number of seasons
        int numOfSeasons = movie.getNumberOfSeasons();
        if (numOfEpisodes > 0) {
            numOfSeasonsTextView.setText(String.valueOf(numOfSeasons));
        } else {
            numOfSeasonsTextView.setText(notAvailableMsg);
        }

    }

    private void updateMovieGeneralInfoView(View view, Movie movie) {
        String notAvailableMessage = getString(R.string.not_available_text_message);

        TextView releaseDateTextView = view.findViewById(R.id.release_date_textview);
        TextView spokenLanguageTextView = view.findViewById(R.id.spoken_language_textview);
        TextView budgetTextView = view.findViewById(R.id.budget_textview);
        TextView revenueTextView = view.findViewById(R.id.revenue_textview);

        // set release date
        String releaseDate = movie.getReleaseDate();
        if (releaseDate != null && !releaseDate.equals(NULL_STRING) && releaseDate.length() > 0)
        releaseDateTextView.setText(releaseDate);

        // set spoken languages
        List<String> spokenLanguagesList = movie.getSpokenLanguage();
        if (spokenLanguagesList != null && spokenLanguagesList.size() > 0) {
            String spokenLanguagesText = TextUtils.join(", ", spokenLanguagesList);
            spokenLanguageTextView.setText(spokenLanguagesText);
        } else {
            spokenLanguageTextView.setText(notAvailableMessage);
        }

        // set budget
        int budget = movie.getBudget();
        if (budget > 0) {
            String budgetText = getString(R.string.movie_budget_revenue_text, budget);
            budgetTextView.setText(budgetText);
        } else {
            budgetTextView.setText(notAvailableMessage);
        }

        // set revenue
        int revenue = movie.getRevenue();
        if (revenue > 0) {
            String revenueText = getString(R.string.movie_budget_revenue_text, revenue);
            revenueTextView.setText(revenueText);
        } else {
            revenueTextView.setText(notAvailableMessage);
        }

    }
}
