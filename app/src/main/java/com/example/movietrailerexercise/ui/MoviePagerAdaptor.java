package com.example.movietrailerexercise.ui;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.utilities.PreferenceUtil;

public class MoviePagerAdaptor extends FragmentPagerAdapter {

    private Context mContext;
    private static boolean sIsTVTabSelected;

    public static final int NOW_PLAYING_TAB = 0;
    public static final int UPCOMING_TAB = 1;
    public static final int POPULAR_TAB = 2;
    public static final int TOP_RATED_TAB = 3;
    public static final int MAXIMUM_NO_OF_PAGES = 4;

    public MoviePagerAdaptor(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return MovieListFragment.newInstance(position, sIsTVTabSelected);
    }

    @Override
    public int getCount() {
        return MAXIMUM_NO_OF_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String pageTitle = null;
        boolean switchToTVFlag = PreferenceUtil.getSwitchMovietoTVFlag(mContext);
        switch (position) {
            case NOW_PLAYING_TAB:
                if (switchToTVFlag)
                    pageTitle = mContext.getString(R.string.airing_today);
                else
                    pageTitle = mContext.getString(R.string.now_playing_title);
                break;
            case UPCOMING_TAB:
                if (switchToTVFlag)
                    pageTitle = mContext.getString(R.string.on_the_air);
                else
                    pageTitle = mContext.getString(R.string.coming_soon_title);
                break;
            case POPULAR_TAB:
                pageTitle = mContext.getString(R.string.popular_title);
                break;
            case TOP_RATED_TAB:
                pageTitle = mContext.getString(R.string.top_rated_title);
                break;
            default:
                break;
        }

        return pageTitle;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public static void setTVTabsSelected(boolean isTVSelected)
    {
        sIsTVTabSelected = isTVSelected;
    }
}
