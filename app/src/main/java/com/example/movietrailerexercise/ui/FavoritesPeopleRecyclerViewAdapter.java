package com.example.movietrailerexercise.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.movietrailerexercise.R;
import com.example.movietrailerexercise.data.MovieContract;
import com.example.movietrailerexercise.utilities.ProviderUtil;

public class FavoritesPeopleRecyclerViewAdapter extends
        RecyclerView.Adapter<FavoritesPeopleRecyclerViewAdapter.FavoritesAdapterViewHolder>
        implements DeleteFavoritesAlertDialog.DeleteMovieListener {

    //private boolean mIsCastTab;
    private Context mContext;
    private Cursor  mCursor;
    private android.support.v7.view.ActionMode mActionMode;
    private android.support.v7.view.ActionMode.Callback mActionModeCallback
            = new android.support.v7.view.ActionMode.Callback()
    {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_menu_delete:
                    //deleteMovie();
                    DeleteFavoritesAlertDialog alertDialog = new DeleteFavoritesAlertDialog();
                    alertDialog.setCallbackListener( FavoritesPeopleRecyclerViewAdapter.this);
                    alertDialog.show( ((AppCompatActivity) mContext).getSupportFragmentManager(),
                            "deleteMoviesAlert");
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {
            mActionMode = null;
        }
    };

    public FavoritesPeopleRecyclerViewAdapter(Context context) {
        mContext = context;
        //mIsCastTab = isCastTab;
    }

    @NonNull
    @Override
    public FavoritesPeopleRecyclerViewAdapter.FavoritesAdapterViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent,
            int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.casts_listview, parent, false);
        view.setFocusable(true);
        return new FavoritesAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull FavoritesPeopleRecyclerViewAdapter.FavoritesAdapterViewHolder holder,
            int position) {

        mCursor.moveToPosition(position);
        int castPortraitPathColIdx =
                mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_PERSON_POSTER_PATH);
        String posterPath = mCursor.getString(castPortraitPathColIdx);
        Glide.with(mContext).load(posterPath).into(holder.mCastPortrait);

        int castNameColIdx =
                mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_PERSON_NAME);
        String castName = mCursor.getString(castNameColIdx);
        holder.mCastName.setText(castName);
    }

    @Override
    public int getItemCount() {
        if (mCursor == null || mCursor.getCount() == 0) {
            return 0;
        }

        return mCursor.getCount();
    }

    @Override
    public void onConfirmDelete() {
        deletePerson();
    }

    private void deletePerson() {
        int personIDColIdx = mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_PERSON_ID);
        int personID = mCursor.getInt(personIDColIdx);
        ProviderUtil.deletePerson(mContext, personID);
    }

    public class FavoritesAdapterViewHolder extends RecyclerView.ViewHolder implements
    View.OnClickListener, View.OnLongClickListener{

        private TextView mCastName;
        private ImageView mCastPortrait;

        public FavoritesAdapterViewHolder(View itemView) {
            super(itemView);
            mCastName = itemView.findViewById(R.id.cast_name);
            mCastPortrait = itemView.findViewById(R.id.cast_portrait);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            mCursor.moveToPosition(adapterPosition);
            int castIDColIdx = mCursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_PERSON_ID);
            int personID = mCursor.getInt(castIDColIdx);
            Intent startCastDetailIntent = new Intent( mContext, PersonDetailActivity.class );
            startCastDetailIntent.putExtra("cast_id", personID);
            mContext.startActivity(startCastDetailIntent);

        }

        @Override
        public boolean onLongClick(View view) {
            if (mActionMode != null) {
                return false;
            }

            int adapterPosition = getAdapterPosition();
            mCursor.moveToPosition(adapterPosition);

            // Start the CAB using the ActionMode.Callback defined above
            mActionMode =  ((AppCompatActivity)mContext)
                    .startSupportActionMode(mActionModeCallback);
            view.setSelected(true);
            return true;
        }
    }

    public void swapCursor( Cursor cursor ) {
        mCursor = cursor;
        notifyDataSetChanged();
    }
}
